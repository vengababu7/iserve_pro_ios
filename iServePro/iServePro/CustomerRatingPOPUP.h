//
//  CustomerRatingPOPUP.h
//  iServePro
//
//  Created by Rahul Sharma on 03/05/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ratingPopDelegate <NSObject>

- (void)popUpRatingOFfDismiss:(NSInteger)tag;

@end

@interface CustomerRatingPOPUP : UIView<UIGestureRecognizerDelegate>
{
    NSInteger reason;
}
@property (weak, nonatomic) id<ratingPopDelegate>delegate;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (assign, nonatomic)BOOL  isSelected;
- (IBAction)submitTheRating:(id)sender;

-(void) onWindow:(UIWindow *)onWindow;

@property (strong, nonatomic) IBOutlet UIButton *cancelReason1;
@property (strong, nonatomic) IBOutlet UIButton *cancelReason3;
@property (strong, nonatomic) IBOutlet UIButton *cancelReason2;
@property (strong, nonatomic) IBOutlet UIButton *cancelReason4;
- (IBAction)reason1:(id)sender;
- (IBAction)reason2:(id)sender;

- (IBAction)reason3:(id)sender;

- (IBAction)reason4:(id)sender;

@end
