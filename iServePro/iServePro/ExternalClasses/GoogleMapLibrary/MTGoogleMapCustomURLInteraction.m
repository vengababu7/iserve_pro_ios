//
//  MTGoogleMapCustomURLInteraction.m
//  MapsTest
//
//  Created by Vinay Raja on 22/02/14.
//  Copyright (c) 2014 3Embed Software Tech Pvt Ltd. All rights reserved.
//

#import "MTGoogleMapCustomURLInteraction.h"

@implementation MTGoogleMapCustomURLInteraction

#pragma mark - URL Encoding Utils

+(NSString*)getPercentEscapeString:(NSString*)string
{
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@"+"];

    NSString *newString = (__bridge NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (__bridge CFStringRef)string, NULL, CFSTR("#[]@!$ &'()*;\"<>%{}|\\^~`"), CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
    
    if (newString)
    {
        return newString;
    }
    
    return @"";
}


+(NSString*)urlEncodedStringFromDictionary:(NSDictionary*)dict
{
    NSMutableString *encodedString = [[NSMutableString alloc] init];
    
    NSArray *paramNames = [dict allKeys];
    NSUInteger paramCount = 0;

    for (NSString *paramName in paramNames)
	{
		NSString *paramValue =  (NSString *)[dict objectForKey:paramName];
        
		if (paramValue != nil)
		{
            NSString * encodedParamName = [self getPercentEscapeString:paramName];
            NSString * encodedParamValue = [self getPercentEscapeString:paramValue];
            
			[encodedString appendFormat:@"%@=%@", encodedParamName,  encodedParamValue];
            
			if (paramCount < ([paramNames count] - 1 ))
			{
				[encodedString appendString:@"&"];
				paramCount++;
			}
		}
	}
    
    return encodedString;
}

#pragma mark - Maps API

+(BOOL)isGoogleMapsAppPresent
{
    return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:GoogleMapsCustomURLScheme]];
}

+(BOOL)openGoogleMaps:(NSURL*)mapURL
{
    if ([self isGoogleMapsAppPresent]) {
        return [[UIApplication sharedApplication] openURL:mapURL];
    }else{
         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/google-maps/id585027354"]];
       // [Helper showAlertWithTitle:@"Message" Message:@"No Google Maps"];
    }
    
    return NO;
}

+(NSString*)createWellFormattedURLForRequest:(NSDictionary*)options allowCallback:(BOOL)callback
{
    NSString *urlString = GoogleMapsCustomURLScheme;
    
    if (callback) {
        urlString = [NSString stringWithFormat:@"%@%@", GoogleMapsCustomURLCallbackScheme, GoogleMapsCustomURLCallbackHost];
    }
    
    if (options && [[options allKeys] count] > 0) {
        urlString = [urlString stringByAppendingString:@"?"];
    }
    else {
        return urlString;
    }
    
    NSMutableDictionary *showMapDict = [NSMutableDictionary dictionaryWithDictionary:options];
    
    if ([options objectForKey:ShowMapKeyMapMode]) {
        ShowMapMapMode mapMode = [[options objectForKey:ShowMapKeyMapMode] integerValue];
        
        NSString *mapModeString = nil;
        switch (mapMode) {
            case MapModeStandard:
                mapModeString = MapModeKeyStandard;
                break;
            case MapModeStreetview:
                mapModeString = MapModeKeyStreetView;
                break;
            default:
                break;
        }
        
        [showMapDict setObject:mapModeString forKey:ShowMapKeyMapMode];
    }
    
    if ([options objectForKey:ShowMapKeyViews]) {
        ShowMapViews mapViews = [[options objectForKey:ShowMapKeyViews] integerValue];
        NSArray *mapViewsArray = @[MapViewsKeySatellite, MapViewsKeyTraffic, MapViewsKeyTransit];
        NSMutableArray *viewsMutableArray = [mapViewsArray mutableCopy];
        
        NSUInteger count = 0;
        while (count < [mapViewsArray count]) {
            if (!(mapViews & (1<<count))) {
                [viewsMutableArray removeObjectIdenticalTo:[mapViewsArray objectAtIndex:count]];
            }
            count++;
            
        }
        
        if ([viewsMutableArray count] > 0) {
            [showMapDict setObject:[viewsMutableArray componentsJoinedByString:@","] forKey:ShowMapKeyViews];
        }
        else {
            [showMapDict removeObjectForKey:ShowMapKeyViews];
        }
        
    }
    
    
    NSString *urlEncodedString = [self urlEncodedStringFromDictionary:showMapDict];
    if (callback) {
        urlString = [urlString stringByAppendingString:[self getCallbackOptionURLString:urlEncodedString]];
    }
    else {
        urlString = [urlString stringByAppendingString:urlEncodedString];
    }

    return urlString;
}

+(BOOL)showMapWithOptions:(NSDictionary*)options allowCallback:(BOOL)callback
{
    NSString *urlString = [self createWellFormattedURLForRequest:options allowCallback:callback];
    
    return [self openGoogleMaps:[NSURL URLWithString:urlString]];

}

+(BOOL)searchWithOptions:(NSDictionary*)options allowCallback:(BOOL)callback
{
    return [self showMapWithOptions:options allowCallback:callback];
}

+(BOOL)showDirections:(NSDictionary*)options allowCallback:(BOOL)callback
{
    
    if (![options objectForKey:DirectionsStartAddress]) {
        return NO;
    }
    
    
    if (![options objectForKey:DirectionsEndAddress]) {
        return NO;
    }
    
    NSMutableDictionary *showDirectionsDict = [NSMutableDictionary dictionaryWithDictionary:options];
    
    NSMutableDictionary *showOptionsDict = [NSMutableDictionary dictionaryWithDictionary:options];

    [showOptionsDict removeObjectForKey:DirectionsStartAddress];
    [showOptionsDict removeObjectForKey:DirectionsEndAddress];
    [showOptionsDict removeObjectForKey:DirectionsDirectionMode];
    
    [showDirectionsDict removeObjectsForKeys:[showOptionsDict allKeys]];
    
    NSString *urlString = [self createWellFormattedURLForRequest:showOptionsDict allowCallback:callback];    
    
    if (![[options objectForKey:DirectionsStartAddress] length]) {
        [showDirectionsDict removeObjectForKey:DirectionsStartAddress];
    }

    
    if ([options objectForKey:DirectionsDirectionMode]) {
        MapDirectionMode directionMode = [[options objectForKey:DirectionsDirectionMode] integerValue];
        
        NSString *directionModeString = nil;
        switch (directionMode) {
            case MapDirectionModeBicycling:
                directionModeString = DirectionModeKeyBicycling;
                break;
            case MapDirectionModeDriving:
                directionModeString = DirectionModeKeyDriving;
                break;
            case MapDirectionModeTransit:
                directionModeString = DirectionModeKeyTransit;
                break;
            case MapDirectionModeWalking:
                directionModeString = DirectionModeKeyWalking;
                break;
            default:
                break;
        }
        
        [showDirectionsDict setObject:directionModeString forKey:DirectionsDirectionMode];
    }

    NSString *urlEncodedString = [self urlEncodedStringFromDictionary:showDirectionsDict];

    urlString = [urlString stringByAppendingFormat:@"&%@",urlEncodedString];
    
    return [self openGoogleMaps:[NSURL URLWithString:urlString]];
}


#pragma mark - Callback Utils

+(NSString*)getDefaultAppName
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
}

+(NSString*)getDefaultSuccessCallbackURL
{
    return [[self getDefaultAppName] stringByAppendingString:@"://?resume=true"];
}

+(NSString*)getDefaultErrorCallbackURL
{
    return [[self getDefaultAppName] stringByAppendingString:@"://"];
}

+(NSString*)getCallbackOptionURLString:(NSString*)url
{
    NSString *appName = [self getDefaultAppName];
    NSString *succesCallBackUrl = [self getDefaultSuccessCallbackURL];
    NSString *errorCallBackUrl = [self getDefaultErrorCallbackURL];
    
    NSString *optionsString = [self urlEncodedStringFromDictionary:@{GoogleMapsCustomURLCallbackSource: appName,
                                                                     GoogleMapsCustomURLCallbackSuccess: succesCallBackUrl,
                                                                     GoogleMapsCustomURLCallbackError: errorCallBackUrl}];
    
    NSString *finalURLString = [NSString stringWithFormat:@"url=%@&%@", url, optionsString];

    return finalURLString;
}

@end
