//
//  ChatSIOClient.m
//  Sup
//
//  Created by Rahul Sharma on 1/8/16.
//  Copyright © 2016 3embed. All rights reserved.
//

#import "ChatSIOClient.h"
#import <SIOSocket/SIOSocket.h>
#import "IServeAppDelegate.h"
#import "ChatSocketIOClient.h"




static ChatSIOClient *sioClient =NULL;

@interface ChatSIOClient()

@property (nonatomic, strong) SIOSocket *socket;
@property (nonatomic, strong) NSString *hostURL;
@property (nonatomic, strong) NSString *portNumber;
@property (nonatomic, strong) ChatSIOConfiguration *configuration;
@property (nonatomic, strong) ChatSocketIOClient *connectSocket;

@end

@interface ChatSIOClient (Utility)

- (NSString*) stringForInput:(id)input;

@end

@implementation ChatSIOClient



+ (instancetype) sharedInstance {
    if (!sioClient) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            sioClient = [ChatSIOClient new];
        });
        
    }
    
    return sioClient;
}

- (void) setConfiguration:(ChatSIOConfiguration*)configuration {
    _configuration = configuration;
}

- (void) connect {
    
    [SIOSocket socketWithHost: [_configuration getHostString] response: ^(SIOSocket *socket) {
        self.socket = socket;
        socket.onConnect = ^() {
            if (_delegate && [_delegate respondsToSelector:@selector(sioClient:didConnectToHost:)]) {
                _isSocketConnected =YES;
                [_delegate sioClient:self didConnectToHost:[self.configuration getHostString]];
            }
        };
        socket.onDisconnect = ^() {
            if (_delegate && [_delegate respondsToSelector:@selector(sioClient:didDisconnectFromHost:)]) {
                _isSocketConnected =NO;
                [_delegate sioClient:self didDisconnectFromHost:[self.configuration getHostString]];
                UIApplicationState state = [[UIApplication sharedApplication] applicationState];
                if (state == UIApplicationStateBackground || state == UIApplicationStateInactive){
                    [self connectTheSocketWhenAppBackGround];
                }
                
            }
        };
        socket.onError = ^(NSDictionary *errorInfo) {
            if (_delegate && [_delegate respondsToSelector:@selector(sioClient:gotError:)]) {
                _isSocketConnected =NO;
                [_delegate sioClient:self gotError:errorInfo];
            }
        };
    }];
}

-(void)connectTheSocketWhenAppBackGround{
    _connectSocket = [ChatSocketIOClient sharedInstance];
    // [_connectSocket socketIOSetup];
}


- (void) disconnect {
    [_socket close];
}

- (void) subscribeToChannels:(NSArray*)channelsArray {
    
    for (NSString *channel in channelsArray ) {
        
        [_socket on:channel callback:^(NSArray *args) {
            
            
            if (_delegate && [_delegate respondsToSelector:@selector(sioClient:didRecieveMessage:onChannel:)]) {
                
                [_delegate sioClient:self didRecieveMessage:args onChannel: channel];
            }
            
        }];
        
        
        if (_delegate && [_delegate respondsToSelector:@selector(sioClient:didSubscribeToChannel:)]) {
            
            [_delegate sioClient:self didSubscribeToChannel:channel];
        }
        
    }
    
}

-(void)publishToChannel:(NSString *)channel message:(id)message{
    
    if (message) {
        
        [_socket emit:channel args:@[message]];
        
        if (_delegate &&[_delegate respondsToSelector:@selector(sioClient:didSendMessageToChannel:)]) {
            [_delegate sioClient:self didSendMessageToChannel:channel];
        }
        
    }
    else{
        
        
        if (_delegate && [_delegate respondsToSelector:@selector(sioClient:gotError:)]) {
            
            [_delegate sioClient:self gotError:@{@"error": @"inavlid message format"}];
        }
        
        
    }
    
    
}

@end


@implementation ChatSIOClient (Utility)

-(NSString *)stringForInput:(id)input{
    NSString *inputString = nil;
    
    if ([input isKindOfClass:[NSDictionary class]] || [input isKindOfClass:[NSArray class]] ){
        
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:input options:NSJSONWritingPrettyPrinted error:nil];
        
        inputString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
        
    }
    
    else if ([input isKindOfClass:[NSString class]]){
        
        inputString = input;
        
    }
    
    return inputString;
    
}

- (void) sioClient:(ChatSIOClient *)client didSendMessageToChannel:(NSString *)channel
{
    NSLog(@"Message updated successfullt to Socket");
}


@end
