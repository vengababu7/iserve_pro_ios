//
//  ChatSIOClient.h
//  Sup
//
//  Created by Rahul Sharma on 1/8/16.
//  Copyright © 2016 3embed. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChatSIOConfiguration.h"

@protocol ChatSIOClientDelegate;




@interface ChatSIOClient : NSObject

@property (nonatomic, assign) BOOL isSocketConnected;

@property (nonatomic,readonly) ChatSIOConfiguration *configuration;

@property (nonatomic,weak) id <ChatSIOClientDelegate> delegate;

-(void)connect;
+(instancetype) sharedInstance;
+(instancetype)defaultConfiguration;

-(void) setConfiguration:(ChatSIOConfiguration *)configuration;

-(void)disconnect;
-(void)subscribeToChannels:(NSArray *)channelsArray;
-(void)publishToChannel:(NSString *)channel message:(id)message;

@end


@protocol ChatSIOClientDelegate<NSObject>

- (void) sioClient:(ChatSIOClient *)client didConnectToHost:(NSString*)host;
- (void) sioClient:(ChatSIOClient *)client didSubscribeToChannel:(NSString*)channel;
- (void) sioClient:(ChatSIOClient *)client didSendMessageToChannel:(NSString *)channel;
- (void) sioClient:(ChatSIOClient *)client didRecieveMessage:(NSArray*)message onChannel:(NSString *)channel;
- (void) sioClient:(ChatSIOClient *)client didDisconnectFromHost:(NSString*)host;
- (void) sioClient:(ChatSIOClient *)client gotError:(NSDictionary *)errorInfo;
@end
