//
//  CustomNavigationBar.m
//  privMD
//
//  Created by Surender Rathore on 15/04/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "CustomNavigationBar.h"
#import "Helper.h"
#import "Fonts.h"

@interface CustomNavigationBar()
@property(nonatomic,strong)UILabel *labelTitle;
@property(nonatomic,strong)UIButton *rightbarButton;
@property(nonatomic,strong)UIButton *leftbarButton;

@end
@implementation CustomNavigationBar
@synthesize labelTitle;
@synthesize rightbarButton;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_screen_navigation_bar.png"]];
        UIView *shadowView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, 320, 6.5)];
        shadowView.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"login_screen_navigation_bar_shadow.png"]];
        [self addSubview:shadowView];
        
        
        labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,20, 320, 40)];
        labelTitle.textColor = [UIColor blackColor];
        labelTitle.textAlignment = NSTextAlignmentCenter;
        
        [Helper setToLabel:labelTitle Text:@"" WithFont:@"Sansation-Regular" FSize:28 Color:UIColorFromRGB(0x3f88a9)];
        
        [self addSubview:labelTitle];
        
        _leftbarButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _leftbarButton.frame = CGRectMake(0,20,66.5,44);
        // [_leftbarButton setTitle:@"MENU" forState:UIControlStateNormal];
        //   [_leftbarButton setTitle:@"MENU" forState:UIControlStateSelected];
        // [_leftbarButton setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateNormal];
        // [_leftbarButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateHighlighted];
        
        _leftbarButton.titleLabel.font = [UIFont fontWithName:Robot_Light size:11];
        UIImage *buttonImage = [UIImage imageNamed:@"appclndrscreen_menu_icon_off.png"];
        [_leftbarButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
        [_leftbarButton setBackgroundImage:[UIImage imageNamed:@"appclndrscreen_menu_icon_on.png"] forState:UIControlStateHighlighted];
        [_leftbarButton addTarget:self action:@selector(leftBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_leftbarButton];
        
        
        
    }
    return self;
}

- (void)addRightBarButton:(BOOL)isHomeView{
    
    //rightbutton
    UIImage *buttonImageRight = [UIImage imageNamed:@"doctor_location_pin_icon_off.png"];
    //rightbutton
    rightbarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightbarButton.frame = CGRectMake(276, 20,40, 40);
      if (isHomeView) {
        [rightbarButton setImage:buttonImageRight forState:UIControlStateNormal];
        [rightbarButton setImage:[UIImage imageNamed:@"doctor_location_pin_icon_on.png"] forState:UIControlStateHighlighted];
     
    }
    
    rightbarButton.tag = 100;
    [rightbarButton addTarget:self action:@selector(rightBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:rightbarButton];

    
}

-(void)setTitle:(NSString*)title{
    // labelTitle.textColor = [UIColor blackColor];
    labelTitle.text = title;
    //[self setNeedsDisplay];
}
-(void)setRightBarButtonTitle:(NSString*)title{
    [rightbarButton setTitle:title forState:UIControlStateNormal];
    //[self setNeedsDisplay];
}
-(void)setLeftBarButtonTitle:(NSString*)title{
    [_leftbarButton setTitle:title forState:UIControlStateNormal];
    
    //[self setNeedsDisplay];
}
-(void)rightBarButtonClicked:(UIButton*)sender{
    if (delegate && [delegate respondsToSelector:@selector(rightBarButtonClicked:)]) {
        [delegate rightBarButtonClicked:sender];
    }
}
-(void)leftBarButtonClicked:(UIButton*)sender {
    if (delegate && [delegate respondsToSelector:@selector(leftBarButtonClicked:)]) {
        [delegate leftBarButtonClicked:sender];
    }
}
@end
