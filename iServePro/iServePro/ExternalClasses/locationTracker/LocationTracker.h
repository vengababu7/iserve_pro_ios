//
//  LocationTracker.h
//  Location
//
//  Created by Surender Rathore
//  Copyright (c) 2014 Location. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "LocationShareModel.h"


typedef void(^LocationTrackerDistanceChangeCallback) (float totalDistance, CLLocation *location);
typedef void(^LocationTrackerDistanceChangeCallbackOnOff)(float totalDistance, CLLocation *location);
@interface LocationTracker : NSObject <CLLocationManagerDelegate>

@property(nonatomic,copy)LocationTrackerDistanceChangeCallback distanceCallback;
@property(nonatomic,copy)LocationTrackerDistanceChangeCallbackOnOff distanceCallbackOnOff;
@property(nonatomic,assign) PubNubStreamAction driverState; // set value for driver state i.e on the way/reached/completed

@property (nonatomic) CLLocationCoordinate2D myLastLocation;
@property (nonatomic) CLLocationAccuracy myLastLocationAccuracy;
@property(nonatomic,strong)CLLocation *lastLocaiton;
@property (strong,nonatomic) NSString * accountStatus;
@property (strong,nonatomic) NSString * authKey;
@property (strong,nonatomic) NSString * device;
@property (strong,nonatomic) NSString * name;
@property (strong,nonatomic) NSString * profilePicURL;
@property (strong,nonatomic) NSNumber * userid;
@property (assign,nonatomic) float distance, distanceOnOff;

@property (strong,nonatomic) LocationShareModel * shareModel;

@property (nonatomic) CLLocationCoordinate2D myLocation;
@property (nonatomic) CLLocationAccuracy myLocationAccuracy;

+ (CLLocationManager *)sharedLocationManager;

- (void)startLocationTracking;
- (void)stopLocationTracking;
- (void)updateLocationToServer;
+ (id)sharedInstance;


@end
