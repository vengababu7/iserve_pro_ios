//
//  HelpViewController.m
//  privMD
//
//  Created by Rahul Sharma on 11/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "iServeHelpController.h"
#import "IServeSignInViewController.h"
#import "Helper.h"
#import "Fonts.h"
#import "SignupViewController.h"


@interface iServeHelpController ()

@end

@implementation iServeHelpController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    self.navigationController.navigationBarHidden = YES;
    self.navigationItem.hidesBackButton = YES;
    self.view.alpha=1.0f;
    [super viewDidLoad];
    
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    
    if ([UIScreen mainScreen].bounds.size.height == 568) {
        
        imageview.image = [UIImage imageNamed:@"default_568h"];
    }
    else if ([UIScreen mainScreen].bounds.size.height == 480)
    {
        imageview.image = [UIImage imageNamed:@"default"];
    }
    else if ([UIScreen mainScreen].bounds.size.height == 667)
    {
        imageview.image = [UIImage imageNamed:@"Default.png"];
        
    }else if ([UIScreen mainScreen].bounds.size.height == 736)
    {
        imageview.image = [UIImage imageNamed:@"default_768h.png"];
    }
    else {
        imageview.image = [UIImage imageNamed:@"default_768h.png"];
    }
    [self.backgroundView addSubview:imageview];
    [self.backgroundView bringSubviewToFront:self.bottomContainerView];}

-(void)viewDidAppear:(BOOL)animated;
{
 [self startAnimationUp];
}
-(void)startAnimationDown
{
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^
     {
         CGRect frame = _bottomContainerView.frame;
         frame.origin.y = [[UIScreen mainScreen] bounds].size.height;
         frame.origin.x = 0;
         _bottomContainerView.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
}
-(void)startAnimationUp
{
    _bottomContainerView.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height, 320, 97);
    
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^
     {
         CGRect frame = _bottomContainerView.frame;
         frame.origin.y = [[UIScreen mainScreen] bounds].size.height-97;
         frame.origin.x = 0;
         _bottomContainerView.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    self.navigationItem.hidesBackButton = YES;
    self.view.alpha=1.0f;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)signInButtonClicked:(id)sender {
    [self startAnimationDown];
    UIStoryboard *mainstoryboard;
    
    if (!mainstoryboard) {
        mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
    IServeSignInViewController  *signInVC = [mainstoryboard instantiateViewControllerWithIdentifier:@"signInVc"];
    
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:signInVC];
    // [NSThread sleepForTimeInterval:1.0 - 0.5f];
    // [UIView animateWithDuration:4.0f
    //                  animations:^ {
    //                    self.view.alpha = 0.3f;
    //           }
    //          completion:^ (BOOL finished) {
    //           [self.navigationController pushViewController:signInVC animated:NO];
    //                     signInVC.view.alpha =0.3f;
    //                         [UIView animateWithDuration:0.8f
    //                                          animations:^ {
    //                                              signInVC.view.alpha = 1.0f;
    //                                          }
    //                                          completion:^ (BOOL finished) {
    //                                          }];
    //
    //       }];
    
    //[self performSegueWithIdentifier:@"SignIn" sender:self];
    [self presentViewController:navBar animated:YES completion:nil];
    
}
- (IBAction)registerButtonClicked:(id)sender {
    [self startAnimationDown];
    UIStoryboard *mainstoryboard;
    
    if (!mainstoryboard) {
        mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
    SignupViewController  *signUpVC = [mainstoryboard instantiateViewControllerWithIdentifier:@"SignUpVc"];
    UINavigationController *navBar1=[[UINavigationController alloc]initWithRootViewController:signUpVC];
    //   [NSThread sleepForTimeInterval:1.0 - 0.5f];
    //   [UIView animateWithDuration:4.0f
    //                   animations:^ {
    //                        self.view.alpha = 0.3f;
    //                  }
    //                    completion:^ (BOOL finished) {
    //                        [self.navigationController pushViewController:signUpVC animated:NO];
    //                     signUpVC.view.alpha =0.3f;
    //                         [UIView animateWithDuration:1.0f
    //                                          animations:^ {
    //                                              signUpVC.view.alpha = 1.0f;
    //                                          }
    //                                          completion:^ (BOOL finished) {
    //                                          }];
    
    //            }];
    // [self performSegueWithIdentifier:@"SignUp" sender:self];
    [self presentViewController:navBar1 animated:YES completion:nil];
}


@end
