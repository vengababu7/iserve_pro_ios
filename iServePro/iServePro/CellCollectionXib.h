//
//  CellCollectionXib.h
//  YaaroDriver
//
//  Created by Rahul Sharma on 05/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellCollectionXib : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *jobImages;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end
