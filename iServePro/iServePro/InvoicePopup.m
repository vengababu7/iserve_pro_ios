//
//  InvoicePopup.m
//  iServePro
//
//  Created by Rahul Sharma on 12/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "InvoicePopup.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation InvoicePopup

static  InvoicePopup *share;


+ (id)sharedInstance
{
    if (!share ) {
        share=[[self alloc]init];
    }
    return share;
}
- (instancetype)init{
    self = [[[NSBundle mainBundle] loadNibNamed:@"invoice"
                                          owner:self
                                        options:nil] firstObject];
    return self;
}
- (void)showPopUpWithDictionary:(NSDictionary *)shipments
                       onWindow:(UIWindow *)window {
    self.frame = window.frame;
    [window addSubview:self];
    _custAddress.text=shipments[@"addrLine1"];
    _custName.text=shipments[@"fname"];
    _phoneNo.text=shipments[@"phone"];
   // _bid.text =[NSString stringWithFormat:@"BID: %@", shipments[@"bid"]];
    NSString *strImageUrl =flStrForObj(shipments[@"sign_url"]);
    [_signatureImg sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                     placeholderImage:[UIImage imageNamed:@""]
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                
                            }];
    
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    
    NSNumber *timeFee =[NSNumber numberWithInteger:[flStrForObj(shipments[@"appt_duration"]) integerValue]*[flStrForObj(shipments[@"price_per_min"]) integerValue]];
    NSString *totTime = [formatter stringFromNumber:timeFee];
    _timeFee.text=totTime;
    
    
    
    NSNumber *mateFee =[NSNumber numberWithInteger:[flStrForObj(shipments[@"mat_fees"]) integerValue]];
    NSString *totalMat = [formatter stringFromNumber:mateFee];
    _matFee.text=totalMat;
    
    NSNumber *miscFee =[NSNumber numberWithInteger:[flStrForObj(shipments[@"misc_fees"]) integerValue]];
    NSString *totalMisc = [formatter stringFromNumber:miscFee];
    _miscFee.text=totalMisc;
    
    NSNumber *proDisc =[NSNumber numberWithInteger:[flStrForObj(shipments[@"pro_disc"]) integerValue]];
    NSString *proDiscount = [formatter stringFromNumber:proDisc];
    _proDisc.text=proDiscount;
    
    NSNumber *disc =[NSNumber numberWithInteger:[flStrForObj(shipments[@"discount"]) integerValue]];
    NSString *discCoupon = [formatter stringFromNumber:disc];
    _disc.text=discCoupon;
    
    
    if ([shipments[@"payment_type"]integerValue]==1) {
        _payType.text=@"CASH";
    }else{
        _payType.text=@"CARD";
    }
    _notes.text=flStrForObj(shipments[@"pro_notes"]);
    
    
    NSNumber *visAmt =[NSNumber numberWithInteger:[flStrForObj(shipments[@"visit_amount"]) integerValue]];
    NSString *visAmount = [formatter stringFromNumber:visAmt];
    _visitFee.text=visAmount;
    
    NSNumber *subTot =[NSNumber numberWithFloat:([timeFee floatValue] + [visAmt floatValue]+[mateFee floatValue]+[miscFee floatValue])];
    NSString *subTotal = [formatter stringFromNumber:subTot];
    _subTot.text=subTotal;
    
    NSNumber *tot =[NSNumber numberWithFloat:([subTot floatValue] - [proDisc floatValue]-[disc floatValue])];
    NSString *total = [formatter stringFromNumber:tot];
    _total.text=total;
    
    [UIView animateWithDuration:0.3
                          delay:0.1
                        options: UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
                         [self layoutIfNeeded];
                     }
                     completion:^(BOOL finished){
                         [self layoutIfNeeded];
                     }];
    
    
}

- (IBAction)closePopup:(id)sender {
    self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    
    [UIView animateWithDuration:0.3
                          delay:0.1
                        options: UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
                     }
                     completion:^(BOOL finished) {
                         
                         [self removeFromSuperview];
                     }];
    
}
@end
