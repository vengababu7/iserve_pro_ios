//
//  CalendarVC.h
//  iServePro
//
//  Created by Rahul Sharma on 28/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JTCalendar/JTCalendar.h>


@interface CalendarVC : UIViewController<JTCalendarDelegate,UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) JTCalendarManager *calendarManager;
@property (weak, nonatomic) IBOutlet JTCalendarMenuView *calendarMenuView;
@property (weak, nonatomic) IBOutlet JTHorizontalCalendarView *calendarContentView;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)previousCal:(id)sender;
- (IBAction)nextCalendar:(id)sender;
@end
