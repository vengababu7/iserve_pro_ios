//
//  AppointmentCollectionViewCell.h
//  iServePro
//
//  Created by Rahul Sharma on 08/10/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppointmentCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imageJob;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndication;
@end
