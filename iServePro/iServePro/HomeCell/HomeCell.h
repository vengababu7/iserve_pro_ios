//
//  HomeScreenCell.h
//  iServepro
//
//  Created by Rahul Sharma on 1/30/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HomeCell;

@protocol HomeCellDelegate <NSObject>

@optional
- (void)refreshAppointmentList:(HomeCell *)cell;
- (void)phoneNumberTapped:(HomeCell *)cell;

@end

@interface HomeCell : UITableViewCell


@property (weak, nonatomic) id<HomeCellDelegate> delegate;
@property (assign, nonatomic) BOOL isAppointmentExpired;
@property (nonatomic) NSDate *startTime;
@property (strong, nonatomic) NSTimer *timer;
- (void) startTimer;
-(void) startTimerAgain;
@property int timeSec, timeMin;

@property (strong, nonatomic) IBOutlet UILabel *custName;
@property (strong, nonatomic) IBOutlet UILabel *noAppointments;

@property (strong, nonatomic) IBOutlet UILabel *bookingStatus;

@property (strong, nonatomic) IBOutlet UILabel *DistanceAway;
@property (strong, nonatomic) IBOutlet UILabel *bookingaddress;
@property (strong, nonatomic) IBOutlet UILabel *requestType;
@property (strong, nonatomic) IBOutlet UIView *divider;
@property (strong, nonatomic) IBOutlet UIImageView *arrowImage;

@property (strong, nonatomic) IBOutlet UILabel *bid;
@end
