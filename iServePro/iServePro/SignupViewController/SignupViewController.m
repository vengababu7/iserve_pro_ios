//
//  SignupViewController.m
//  iServePro
//
//  Created by Rahul Sharma on 28/06/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "SignupViewController.h"
#import "TermsNConditions.h"
#import "ProviderTableViewController.h"
#import "CitySelectTableViewController.h"
#import "CountryNameTableViewController.h"
#import "iServeHelpController.h"
#import "AmazonTransfer.h"

#define REGEX_PASSWORD_ONE_UPPERCASE @"^(?=.*[A-Z]).*$"  //Should contains one or more uppercase letters
#define REGEX_PASSWORD_ONE_LOWERCASE @"^(?=.*[a-z]).*$"  //Should contains one or more lowercase letters
#define REGEX_PASSWORD_ONE_NUMBER @"^(?=.*[0-9]).*$"  //Should contains one or more number
#define REGEX_PASSWORD_ONE_SYMBOL @"^(?=.*[!@#$%&_]).*$"  //Should contains one or more symbol

@interface SignupViewController ()<ProviderTableDelegate,SelectedCityDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,CLLocationManagerDelegate>

@property (strong, nonatomic) UIImage *pickedImage;
@property(nonatomic,assign)NSUInteger countProvider;
@property CGFloat initialOffsetY;
@property (assign ,nonatomic) BOOL isKeyboardIsShown;
@property(nonatomic,strong)CLLocationManager *locationManager;
@property(nonatomic,assign)float currentLatitude;
@property(nonatomic,assign)float currentLongitude;
@property(nonatomic,assign)NSString *_profileIMGURL;

@end

@implementation SignupViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    _countryCode.text=@"+91";
    [self createNavLeftButton];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton           = NO;
    self.navigationController.navigationBar.topItem.title =@"Register";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: UIColorFromRGB(0x2598ed)};
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"OpenSans-regular" size:18.0],NSFontAttributeName,
      nil]];
    _custNameTF.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    _lastNameTF.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    _locationManager.delegate=self;
    _initialOffsetY=_mainScrollView.contentOffset.y;
    isTnCButtonSelected = NO;
}
-(void)viewWillAppear:(BOOL)animated{
    self.view.alpha=1.0f;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    _isKeyboardIsShown = NO;

}
- (void)viewDidAppear:(BOOL)animated{
    
    [self getCurrentLocation];
}

- (void)viewWillDisappear:(BOOL)animated
{
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*-----------------------------------*/
#pragma mark - Navigation Bar methods
/*-----------------------------------*/
- (void)createNavLeftButton
{
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton setImage:[UIImage imageNamed:@"current_booking_cross_icon_off"]
                     forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:@"current_booking_cross_icon_on"]
                     forState:UIControlStateSelected];
    
    [navCancelButton addTarget:self action:@selector(backToController) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(10.0f,0.0f,40,40)];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -14;// it was -6 in iOS 6
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void) keyboardWillShow:(NSNotification *)note
{
    
 //   CGSize keyboardSize = [[[note userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGSize keyboardSize = CGSizeMake(320, 216);
    CGFloat height = UIDeviceOrientationIsPortrait([[UIDevice currentDevice] orientation]) ? keyboardSize.height : keyboardSize.width;
    
    [UIView animateWithDuration:0.4 animations:^{
        UIEdgeInsets edgeInsets = [self.mainScrollView contentInset];
        edgeInsets.bottom = height;
        [self.mainScrollView setContentInset:edgeInsets];
        edgeInsets = [self.mainScrollView scrollIndicatorInsets];
        edgeInsets.bottom = height;
        [self.mainScrollView setScrollIndicatorInsets:edgeInsets];
    }];    _isKeyboardIsShown = YES;

}


-(void) keyboardWillHide:(NSNotification *)note
{
    [UIView animateWithDuration:0.4 animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        [self.mainScrollView setContentInset:edgeInsets];
        [self.mainScrollView setScrollIndicatorInsets:edgeInsets];
    }];

}
#pragma mark - 


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([textField isEqual:_providerTF]||[textField isEqual:_cityTF]) {
        [self moveViewDown];
        [self.view endEditing:YES];
        _mainScrollView.scrollEnabled=NO;
        return NO;
    }
    else if( [textField isEqual:_passwordTF] || [textField isEqual:_confirmpasswordTF])
    {
       // [self moveViewUp:textField andKeyboardHeight:200];
        _mainScrollView.scrollEnabled=YES;
        
    }else if([textField isEqual:_phoneTF]||[textField isEqual:_licenceNumber]){
        _mainScrollView.scrollEnabled=YES;
      //  [self moveViewUp:textField andKeyboardHeight:200];
    }
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == _confirmpasswordTF)
    {
        int strength = [self checkPasswordStrength:_passwordTF.text];
        if(strength == 0)
        {
            [_passwordTF becomeFirstResponder];
        }
    }
    
    if (textField!= _emailTF &&[_emailTF.text length] > 0 && ![Helper emailValidationCheck:_emailTF.text]) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Invalid Email Id", @"Invalid Email Id")];
        
        [_emailTF becomeFirstResponder];
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}


- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == _custNameTF)
    {
        [_lastNameTF becomeFirstResponder];
    }
    else if(textField == _lastNameTF)
        
    {
        [_emailTF becomeFirstResponder];
    }
    else if (textField == _emailTF)
    {
        [_phoneTF becomeFirstResponder];
    }
    
    else if (textField == _phoneTF)
    {
        [self selectProCity:nil];
    }
    
    else if (textField == _cityTF)
    {
        //[_passwordTF becomeFirstResponder];
    }
    else if (textField == _licenceNumber)
    {
        [_passwordTF becomeFirstResponder];
    }
    
    else if (textField == _passwordTF)
    {
        int strength = [self checkPasswordStrength:_passwordTF.text];
        if(strength == 0)
        {
            [_passwordTF becomeFirstResponder];
        }
        else
        {
            [_confirmpasswordTF becomeFirstResponder];
        }
    }
    
    else
    {
        [_confirmpasswordTF resignFirstResponder];
        [self signupAction:nil];
        
    }
    return YES;
}
#pragma mark - Password Checking

-(int)checkPasswordStrength:(NSString *)password
{
    unsigned long int  len = password.length;
    //will contains password strength
    int strength = 0;
    
    if (len == 0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Please enter password first" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        alert.tag=1;
        [alert show];
        
        return 0;//PasswordStrengthTypeWeak;
    } else if (len <= 5) {
        strength++;
    } else if (len <= 10) {
        strength += 2;
    } else{
        strength += 3;
    }
    int kp = strength;
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_UPPERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Please enter atleast one Uppercase alphabet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        alert.tag=1;
        [alert show];
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_LOWERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Please enter atleast one Lowercase alphabet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        alert.tag=1;
        [alert show];
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_NUMBER caseSensitive:YES];
    if (kp >= strength) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Please enter atleast  one Number" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        alert.tag=1;
        [alert show];
        
        return 0;
    }
    return 1;
}

- (int)validateString:(NSString *)string withPattern:(NSString *)pattern caseSensitive:(BOOL)caseSensitive
{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:((caseSensitive) ? 0 : NSRegularExpressionCaseInsensitive) error:&error];
    
    NSAssert(regex, @"Unable to create regular expression");
    
    NSRange textRange = NSMakeRange(0, string.length);
    //NSLog(@"test range %ld",textRange);
    NSRange matchRange = [regex rangeOfFirstMatchInString:string options:NSMatchingReportProgress range:textRange];
    
    BOOL didValidate = 0;
    
    // Did we find a matching range
    if (matchRange.location != NSNotFound)
        didValidate = 1;
    
    return didValidate;
}

- (IBAction)selectThePic:(id)sender {
    [self.view endEditing:YES];
    UIActionSheet *actionSheet;
    if (!_pickedImage) {
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select Picture", @"Select Picture") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Take Photo", @"Take Photo"),NSLocalizedString(@"Choose From Library", @"Choose From Library"), nil];
    }else{
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select Picture", @"Select Picture") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Take Photo", @"Take Photo"),NSLocalizedString(@"Choose From Library", @"Choose From Library"),NSLocalizedString(@"Remove Photo", @"Remove Photo"), nil];
    }
    
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 1)
    {
        switch (buttonIndex)
        {
            case 0:
            {
                [self cameraButtonClicked:nil];
                break;
            }
            case 1:
            {
                [self libraryButtonClicked:nil];
                break;
            }
            case 2:{
                [self removePhoto];
                return;
            }
            default:
                break;
        }
    }
}

-(void)libraryButtonClicked:(id)sender
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.allowsEditing = YES;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
    } else {
        
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)removePhoto{
    _pickedImage = nil;
    _profileImage.image = [UIImage imageNamed:@"my_profile_profile_default_image.png"];
}

-(void)cameraButtonClicked:(id)sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate =self;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message: @"Camera is not available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    _profileImage.image = image;
    
    CGSize size = CGSizeMake(125, 125);
    _pickedImage = [self scaleImage:image toSize:size];
}

- (UIImage *)scaleImage:(UIImage *)image toSize:(CGSize)size
{
    
    UIGraphicsBeginImageContextWithOptions(size,NO,0.0);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
    
}


- (IBAction)selectProCity:(id)sender {
    [self performSegueWithIdentifier:@"toCities" sender:self];
}


- (IBAction)selectProvider:(id)sender {
    if (![[NSUserDefaults standardUserDefaults]objectForKey:@"city_ID"]) {
        [Helper showAlertWithTitle:@"Alert" Message:@"Please select the City"];
    }else{
    [self performSegueWithIdentifier:@"toProvider" sender:self];
    }
}


- (IBAction)selectCountryCode:(id)sender {
    CountryNameTableViewController *pickController = [self.storyboard instantiateViewControllerWithIdentifier:@"countryPicker"];
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:pickController];
    pickController.oncomplete = ^(NSString  *code, UIImage *flagimg, NSString *countryName)
    {
        self.flagImage.image=flagimg;
        NSString *countryCode = [NSString stringWithFormat:@"+%@", code];
        self.countryCode.text = countryCode;
        [_phoneTF becomeFirstResponder];
    };
    [self presentViewController:navBar animated:YES completion:nil];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"toProvider"]) {
        ProviderTableViewController *PTVC =[segue destinationViewController];
        PTVC.delegate=self;
    }
    else if ([segue.identifier isEqualToString:@"toCities"]){
        
        CitySelectTableViewController *CSTV = [segue destinationViewController];
        CSTV.delegate = self;
    }
}

- (IBAction)checkButton:(id)sender {
    UIButton *mBut = (UIButton *)sender;
    mBut.userInteractionEnabled = YES;
    if(mBut.isSelected)
    {
        isTnCButtonSelected = NO;
        mBut.selected=NO;
        [_checkButton setImage:[UIImage imageNamed:@"btn_checkbox_normal.png"] forState:UIControlStateNormal];
    }
    else
    {
        isTnCButtonSelected = YES;
        mBut.selected=YES;
        [_checkButton setImage:[UIImage imageNamed:@"btn_checkbox_pressed.png"] forState:UIControlStateSelected];
    }

}

- (IBAction)termsNConditons:(id)sender {
    UIStoryboard *mainstoryboard;
    
    if (!mainstoryboard) {
        mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
    TermsNConditions  *tncVC = [mainstoryboard instantiateViewControllerWithIdentifier:@"tncVC"];
      UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:tncVC];
    //   [NSThread sleepForTimeInterval:1.0 - 0.5f];
//    [UIView animateWithDuration:1.0f
 //                    animations:^ {
                         //               self.view.alpha = 0.3f;
   //                  }
     //                completion:^ (BOOL finished) {
       //                  [self.navigationController pushViewController:tncVC animated:NO];
                         //                tncVC.view.alpha =0.3f;
                         //                         [UIView animateWithDuration:1.0f
                         //                                          animations:^ {
                         //      //                                        tncVC.view.alpha = 1.0f;
                         //                                          }
                         //                                          completion:^ (BOOL finished) {
                         //                                          }];
                         
         //            }];
     [self presentViewController:navBar animated:YES completion:nil];
    
}
-(void)dismissKeyboard
{
    [self.view endEditing:YES];
    [_custNameTF resignFirstResponder];
    [_lastNameTF resignFirstResponder];
    [_providerTF resignFirstResponder];
    [_cityTF resignFirstResponder];
    [_confirmpasswordTF resignFirstResponder];
    [_phoneTF resignFirstResponder];
    [_passwordTF resignFirstResponder];
    [_licenceNumber resignFirstResponder];
}

#pragma mark - cityTypeDelegate method

-(void)SelectedCity:(NSString *)city
{
    _providerTF.text = @"";
    _cityTF.text = city;
    
}
#pragma mark - providerTypeDelegate method

-(void)providerSelectedTypes:(NSArray *)types
{
    NSString *typeCat = @"";
    _countProvider =types.count;
    for (NSString *cat in types) {
        
        if (_countProvider == 1) {
            typeCat = [typeCat stringByAppendingFormat:@"%@",cat];
        }else
        {
            typeCat = [typeCat stringByAppendingFormat:@"%@,",cat];
        }
        _countProvider--;
    }
    _providerTF.text = typeCat;
    [_licenceNumber becomeFirstResponder];
}
/**
 *  Scroll up when keyboard appears
 */
- (void)moveViewUp:(UITextField *)textfield andKeyboardHeight:(float)height
{
    float viewMaxY = CGRectGetMinY([[[[textfield superview] superview] superview] superview].frame);
    viewMaxY = viewMaxY + CGRectGetMinY([[[textfield superview] superview] superview].frame);
    viewMaxY = viewMaxY + CGRectGetMinY([[textfield superview] superview] .frame);
    viewMaxY = viewMaxY + CGRectGetMinY([textfield superview].frame);
    viewMaxY = viewMaxY + CGRectGetMaxY(textfield.frame);
    
    float textfieldMaxY = CGRectGetMinY(_contentScrollView.frame) + viewMaxY + height - _initialOffsetY;
    float reminder = textfieldMaxY - CGRectGetHeight(self.view.frame);
    
    
    NSLog(@"Reminder : %f",reminder);
    
    float offsetY;
    if (reminder > 0)
        offsetY = _initialOffsetY + reminder;
    else
        offsetY = _initialOffsetY;
    
    NSLog(@"OffsetY : %f",offsetY);
    
    [UIView animateWithDuration:0.4
                     animations:^{
                         _mainScrollView.contentOffset = CGPointMake(0, offsetY);
                     }];
}
/**
 *  Scroll up when keyboard hides
 */

- (void)moveViewDown
{
    
    [UIView animateWithDuration:0.4
                     animations:^{
                         CGPoint offset;
                         offset.x = 0;
                         offset.y = 0;
                         _mainScrollView.contentOffset = offset;
                     }];
    
}
-(void)getCurrentLocation{
    
    if ([CLLocationManager locationServicesEnabled]) {
        
        if (!_locationManager) {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                
                [_locationManager requestAlwaysAuthorization];
            }
            _locationManager.distanceFilter = kCLDistanceFilterNone;
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            
        }
        [_locationManager startUpdatingLocation];
        
        
    }
    else {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Service" message:@"Unable to find your location,Please enable location services." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
        
    }
    
}
#pragma mark -CLLocation manager deleagte method
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation __OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_6, __MAC_NA, __IPHONE_2_0, __IPHONE_6_0){
    
    _currentLatitude= newLocation.coordinate.latitude;
    _currentLongitude= newLocation.coordinate.longitude;
    [_locationManager stopUpdatingLocation];
    
}
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    
}
-(void)backToController
{
   // [self.navigationController popViewControllerAnimated:YES];
    
       [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)signupAction:(id)sender {
    [self signUp];
}

-(void)signUp
{
    [self.view endEditing:YES];
    
    NSString *signupFirstName       = _custNameTF.text;
    NSString *signupIServeType      = _providerTF.text;
    NSString *signupPassword        = _passwordTF.text;
    NSString *signupConfirmPassword = _confirmpasswordTF.text;
    NSString *signupPhoneno         = _phoneTF.text;
    NSString *signupEmail           = _emailTF.text;
    NSString *signupCityType        = _cityTF.text;
    NSString *licenceNo             =_licenceNumber.text;
    
    
    
    
    if ((unsigned long)signupFirstName.length == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Enter First Name"];
        [_custNameTF becomeFirstResponder];
    }
    
    else if ((unsigned long)signupEmail.length == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Enter Email ID"];
        [_emailTF becomeFirstResponder];
    }
    else if ((unsigned long)signupPhoneno.length == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Enter Phone No"];
        [_phoneTF becomeFirstResponder];
    }
    
    else if ((unsigned long)signupCityType.length == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Select the city"];
        [_licenceNumber becomeFirstResponder];
    }
    else if ((unsigned long)signupIServeType.length == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Select the Provider Type"];
    }
    else if ((unsigned long)licenceNo.length == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Please Provide Licence Number"];
    }
    else if ((unsigned long)signupPassword.length == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Enter Password"];
        [_passwordTF becomeFirstResponder];
    }
    else if ([Helper emailValidationCheck:signupEmail] == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Invalid Email Id"];
        _emailTF.text = @"";
        [_emailTF becomeFirstResponder];
    }
    else if ([signupPassword isEqualToString:signupConfirmPassword] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Password Mismatched" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        alert.tag=2;
        [alert show];
        _confirmpasswordTF.text       = @"";
        [_confirmpasswordTF becomeFirstResponder];
    }
    
    else if (!isTnCButtonSelected)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Please select our Terms and Conditions"];
        
    }
    else
    {
        if ((unsigned long)_lastNameTF.text.length == 0)
        {
            _lastNameTF.text = @"";
        }
        
        NSString * pushToken;
        if([[NSUserDefaults standardUserDefaults]objectForKey:kSMPgetPushToken])
        {
            pushToken =[[NSUserDefaults standardUserDefaults]objectForKey:kSMPgetPushToken];
            
        }
        else
        {
            pushToken =@"dgfhfghr765998ghghj";
            
        }
        
        NSString *deviceID ;
        
        if (IS_SIMULATOR) {
            deviceID = kPMDTestDeviceidKey;
        }
        else {
            deviceID = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
        }
        
        
        ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
        [progressIndicator showPIOnView:self.view withMessage:@"Signing Up.."];
        
        NSString * lattitude =[NSString stringWithFormat:@"%f",_currentLatitude];
        NSString * logitude =[NSString stringWithFormat:@"%f",_currentLongitude];
        
        if (_pickedImage!=nil ) {
            
            [self uploadImage:^(BOOL success, NSString *imageUrl) {
                
                NSString *ImageUrl = @"";
                if (success) {
                    ImageUrl = imageUrl;
                }
                
                NSDictionary *queryParams =@{
                                             
                                             kSMPSignUpFirstName :signupFirstName,
                                             kSMPSignUpLastName :_lastNameTF.text,
                                             kSMPSignUpEmail :signupEmail,
                                             kSMPSignUpPassword :signupPassword,
                                             kSMPSignUpMobile :[NSString stringWithFormat:@"%@%@",_countryCode.text,signupPhoneno],
                                             @"ent_zipcode" :@"123221",
                                             @"ent_city_id"  :[[NSUserDefaults standardUserDefaults]objectForKey:@"city_ID"],
                                             @"ent_device_type_arr":[[NSUserDefaults standardUserDefaults]objectForKey:@"ProviderIDs"],
                                             @"ent_license_num":_licenceNumber.text,
                                             @"ent_fees_group":[[NSUserDefaults standardUserDefaults]objectForKey:@"type"],
                                             kSMPSignUpDeviceId :deviceID,
                                             kSMPgetPushToken :pushToken,
                                             kSMPSignUpDeviceType :@"1",
                                             kSMPSignUpLattitude :lattitude,
                                             kSMPSignUpLongitude :logitude,
                                             kSMPSignUpDateTime :[Helper getCurrentDateTime],
                                             @"ent_app_version":@"1.0",
                                             @"ent_pro_image":__profileIMGURL
                                             };
                
                TELogInfo(@"param%@",queryParams);
                
                NetworkHandler *handler = [NetworkHandler sharedInstance];
                
                [handler composeRequestWithMethod:MethodPatientSignUp
                                          paramas:queryParams
                                     onComplition:^(BOOL succeeded, NSDictionary *response) {
                                         [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                         
                                         if (succeeded) {
                                             [self signupResponse:response];
                                         }
                                         else{
                                             NSLog(@"Error");
                                             self.navigationItem.leftBarButtonItem.enabled = YES;
                                         }
                                     }];
            }];
        }else{
            NSDictionary *queryParams =@{
                                         
                                         kSMPSignUpFirstName :signupFirstName,
                                         kSMPSignUpLastName :_lastNameTF.text,
                                         kSMPSignUpEmail :signupEmail,
                                         kSMPSignUpPassword :signupPassword,
                                         kSMPSignUpMobile :[NSString stringWithFormat:@"%@%@",_countryCode.text,signupPhoneno],
                                         @"ent_zipcode" :@"123221",
                                         @"ent_city_id"  :[[NSUserDefaults standardUserDefaults]objectForKey:@"city_ID"],
                                         @"ent_device_type_arr":[[NSUserDefaults standardUserDefaults]objectForKey:@"ProviderIDs"],
                                         @"ent_license_num":_licenceNumber.text,
                                         kSMPSignUpDeviceId :deviceID,
                                         kSMPgetPushToken :pushToken,
                                         kSMPSignUpDeviceType :@"1",
                                         kSMPSignUpLattitude :lattitude,
                                         kSMPSignUpLongitude :logitude,
                                         kSMPSignUpDateTime :[Helper getCurrentDateTime],
                                         @"ent_app_version":@"1.0",
                                         @"ent_fees_group":[[NSUserDefaults standardUserDefaults]objectForKey:@"type"],
                                         @"ent_pro_image":@""
                                         };
            
            TELogInfo(@"param%@",queryParams);
            
            NetworkHandler *handler = [NetworkHandler sharedInstance];
            
            [handler composeRequestWithMethod:MethodPatientSignUp
                                      paramas:queryParams
                                 onComplition:^(BOOL succeeded, NSDictionary *response) {
                                     [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                     
                                     if (succeeded) {
                                         [self signupResponse:response];
                                     }
                                     else{
                                         NSLog(@"Error");
                                         self.navigationItem.leftBarButtonItem.enabled = YES;
                                     }
                                 }];
        }
    }
}

/**
 *  upload the image in amazon
 *
 *  @param profile image of received person
 */
-(void)uploadImage:(void (^)(BOOL success, NSString *imageUrl))completed
{
    
    NSString *name = [NSString stringWithFormat:@"%@.jpg",_emailTF.text];
    
    NSString *fullImageName = [NSString stringWithFormat:@"ProfileImages/%@",name];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddhhmmssa"];
    
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[formatter stringFromDate:[NSDate date]]]];
    
    NSData *data = UIImageJPEGRepresentation(_pickedImage,0.8);
    [data writeToFile:getImagePath atomically:YES];
    
    
    [AmazonTransfer upload:getImagePath
                   fileKey:fullImageName
                  toBucket:Bucket
                  mimeType:@"image/jpeg"
           completionBlock:^(AWSS3TransferUtilityUploadTask *task, NSError *error){
               
               if (!error) {
                   NSLog(@"Uploaded Profile Image:%@",[NSString stringWithFormat:@"https://s3.amazonaws.com/%@/%@",Bucket,fullImageName]);
                   __profileIMGURL =[NSString stringWithFormat:@"https://s3.amazonaws.com/%@/%@",Bucket,fullImageName];
                   completed(YES,__profileIMGURL);
                   
               }else{
                   NSLog(@"Photo Upload Failed");
                   completed(NO,nil);
               }
               
           }
     ];
    
}


/**
 *  get the current date
 *
 *  @return returns formatted time
 */
-(NSString *)getCurrentTimeDate
{
    
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd-HH:mm:ss"];
    NSString *dateInStringFormated = [dateFormatter stringFromDate:now];
    NSLog(@"date in string %@ ",dateInStringFormated);
    return dateInStringFormated;
    
}

- (void)signupResponse:(NSDictionary *)response{
    
    
    NSDictionary *dictResponse  = response;
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    if (!response)
    {
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }else if (response[@"error"]){
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"error" Message:[response objectForKey:@"error"]];
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }else if ([[dictResponse objectForKey:@"errFlag"] intValue] == 1){
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"errMsg"]];
    }
    else
    {
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"city_ID"];
            [Helper showAlertWithTitle:@"Thank You!!!"
                               Message:[response objectForKey:@"errMsg"]];
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            [ud setObject:response[@"token"] forKey:KDAcheckUserSessionToken];
            [ud setObject:response[@"email"] forKey:kNSUDoctorEmailAddressKey];
            [ud setObject:response[@"name"] forKey:kNSUDoctorNameKey];
            [ud setObject:response[@"fName"] forKey:kNSUDoctorNameKey];
            [ud setObject:response[@"serverChn"] forKey:kNSUDoctorServerChanelKey];
            [ud setObject:response[@"phone"] forKey:kNSUDoctorPhonekey];
            [ud setObject:@"Profilepic.png" forKey:kNSUDoctorProfilePicKey];
            [ud setObject:response[@"typeId"] forKey:@"proType"];
            [ud setValue:@"4" forKey:@"DoctorStatus"];
            [ud setObject:@"1" forKey:@"signing"];
            [pi hideProgressIndicator];
            [self gotoHomeScreen];
        }
        else{
            [pi hideProgressIndicator];
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
        }
        
    }
}

-(void)gotoHomeScreen{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    iServeHelpController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"helpVC"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
}

@end
