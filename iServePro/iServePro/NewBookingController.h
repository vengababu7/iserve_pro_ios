//
//  NewBookingController.h
//  iServePro
//
//  Created by Rahul Sharma on 03/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PICircularProgressView.h"

@interface NewBookingController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *custLoc1;

@property (strong, nonatomic) NSDictionary *detailsDict;
@property (assign, nonatomic) double timeRemaining;
@property (assign, nonatomic) BOOL isTimeOver;

@property (strong, nonatomic) IBOutlet UILabel *requestType;
@property (strong, nonatomic) IBOutlet UIImageView *mapImage;
- (IBAction)acceptOrder:(id)sender;
- (IBAction)rejectOrder:(id)sender;
@property (strong, nonatomic) IBOutlet PICircularProgressView *progressView;
- (void)receiveNotification:(NSDictionary *)dict;
@end
