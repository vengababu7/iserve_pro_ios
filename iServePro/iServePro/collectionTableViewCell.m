//
//  collectionTableViewCell.m
//  iServePro
//
//  Created by Rahul Sharma on 22/08/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "collectionTableViewCell.h"
#import "ProfileCollectionViewCell.h"
#import "AddImagesCollectionViewCell.h"
#import "ImageViewCollection.h"
#import "AmazonTransfer.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ProfileViewController.h"

@implementation collectionTableViewCell
{
    ImageViewCollection *collectionImages;
    ProfileViewController *callService;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    _count =0;
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
#pragma uicollectionView delegates
-(void)addImagesSelected{
    if (_count==0) {
        _count=1;
    }else{
        _count=0;
        [self uploadToAmazon];
        [self updateTheCount];
    }
    [self.jobCollection reloadData];
}

-(void)uploadToAmazon{
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearMemory];
    [imageCache clearDisk];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[ProgressIndicator sharedInstance]showPIOnView:self withMessage:@"Uploading Images.."];
        for (int i = _count1; i<_images.count+_count1; i++) {
            NSString *name = [NSString stringWithFormat:@"%@_%d.png",[[NSUserDefaults standardUserDefaults] objectForKey:@"proid"],i];
            
            NSString *fullImageName = [NSString stringWithFormat:@"jobImages/%@",name];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyyMMddhhmmssa"];
            
            NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[formatter stringFromDate:[NSDate date]]]];
            
            NSData *data = UIImageJPEGRepresentation(_images[i-_count1],0.8);
            [data writeToFile:getImagePath atomically:YES];
            
            [AmazonTransfer upload:getImagePath
                           fileKey:fullImageName
                          toBucket:Bucket
                          mimeType:@"image/jpeg"
                   completionBlock:^(AWSS3TransferUtilityUploadTask *task, NSError *error){
                       
                       if (!error) {
                           NSLog(@"Uploaded Profile Image:%@",[NSString stringWithFormat:@"https://s3.amazonaws.com/%@/%@",Bucket,fullImageName]);
                       }else{
                           NSLog(@"Photo Upload Failed");
                       }
                   }
             ];
        }
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
    });
}

-(void)updateTheCount{
    for (int i=0; i<_images.count; i++) {
        [_UpdatedImages addObject:_images[i]];
    }
    if (_UpdatedImages.count==0) {
        _count1=0;
        
    }
    NSDictionary *dict =@{
                          @"ent_sess_token":[[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                          @"ent_dev_id":[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                          @"ent_num_job_img":[NSString stringWithFormat:@"%d",_UpdatedImages.count],
                          @"ent_date_time":[Helper getCurrentDateTime]
                          };
    NetworkHandler *handler =[NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"updateMasterProfile"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 [_images removeAllObjects];
                                 callService =[[ProfileViewController alloc]init];
                                 [callService getProfileData];
                                 NSLog(@"succeeded");
                             }
                         }];
}


-(void)reloadCollectionView:(NSMutableArray*)jobImages{
    _isAddOrDelete =YES;
    _images = jobImages ;
    [self.jobCollection reloadData];
}


-(void)reloadCollection:(NSInteger )jobImages{
    _cacheImages = [[NSMutableArray alloc]init];
    _count1=jobImages;
    _isAddOrDelete = NO;
    for (int i=0; i<jobImages; i++) {
        NSString *strImageUrl;
        strImageUrl =[NSString stringWithFormat:@"https://s3.amazonaws.com/iserve/jobImages/%@_%d.png",[[NSUserDefaults standardUserDefaults] objectForKey:@"proid"],i] ;
        [_UpdatedImages addObject:strImageUrl];
    }
    
    [self.jobCollection reloadData];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 2;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if(section == 1){
        return _count;
    }else
    {
        if (!self.images.count){
            return  _UpdatedImages.count;;
        }else{
            
            return self.images.count+_count1;
        }
    }
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    if(indexPath.section == 0){
        cellIdentifier = @"images";
        ProfileCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        if (_count1>indexPath.row) {
            //              static dispatch_once_t onceToken;
            //            dispatch_once(&onceToken, ^{
            //                SDImageCache *imageCache = [SDImageCache sharedImageCache];
            //                [imageCache clearMemory];
            //                [imageCache clearDisk];
            //            });
            NSString *strImageUrl =_UpdatedImages[indexPath.row];
            [cell.activityIndicator startAnimating];
            
            [cell.jobImages sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                              placeholderImage:[UIImage imageNamed:@"user_image_default"] options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                  [cell.activityIndicator stopAnimating];
                                  [_cacheImages addObject:cell.jobImages.image];
                              }];
            
            
            //            [cell.jobImages sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
            //                              placeholderImage:[UIImage imageNamed:@"user_image_default"]
            //                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
            //                                         [cell.activityIndicator stopAnimating];
            //                                        //   [_cacheImages addObject:cell.jobImages.image];
            //                                     }];
            
        }else{
            if (_images.count) {
                
                cell.jobImages.image =self.images[indexPath.row-_count1];
                //  [_cacheImages addObject:cell.jobImages.image];
            }else{
                
                cell.jobImages.image =_UpdatedImages[indexPath.row];
                //  [_cacheImages addObject:cell.jobImages.image];
            }
        }
        return cell;
    }else
    {
        cellIdentifier = @"addImages";
        
        AddImagesCollectionViewCell *addPhotosButtonCell = (AddImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        return addPhotosButtonCell;
        
    }
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    collectionImages= [ImageViewCollection sharedInstance];
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    NSInteger profileTag =1;
    [collectionImages showPopUpWithDictionary:window jobImages:_count1 index:indexPath tag:profileTag];
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                                            action:@selector(activateDeletionMode:)];
    longPress.delegate = self;
    [collectionView addGestureRecognizer:longPress];
}

- (void)activateDeletionMode:(UILongPressGestureRecognizer *)gr
{
    
    if (gr.state == UIGestureRecognizerStateBegan) {
        if (!_isDeleteActive) {
            NSIndexPath *indexPath = [_jobCollection indexPathForItemAtPoint:[gr locationInView:_jobCollection]];
            UICollectionViewCell *cell = [_jobCollection cellForItemAtIndexPath:indexPath];
            _deletedIndexpath = indexPath.row;
            [cell addSubview:_buttonDelete];
            [_buttonDelete bringSubviewToFront:_jobCollection];
            [self delete:_buttonDelete];
        }
    }
}
- (void)delete:(UIButton *)sender
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Confirm", @"Confirm")
                                                        message:NSLocalizedString(@"Are you sure to delete the image?",@"Are you sure to delete the image?")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"No", @"No")
                                              otherButtonTitles:NSLocalizedString(@"Yes", @"Yes"), nil];
    [alertView show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        _isAddOrDelete =YES;
        [_UpdatedImages removeObjectAtIndex:_deletedIndexpath];
        [_buttonDelete removeFromSuperview];
        [self uploadToAmazonAfterDelete];
        [self updateTheCount];
    }
}


-(void)uploadToAmazonAfterDelete{
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearMemory];
    [imageCache clearDisk];
    
    [[ProgressIndicator sharedInstance]showPIOnView:self withMessage:@"Deleting Image.."];
    
    for (int i = 0; i<_UpdatedImages.count; i++) {
        [[ProgressIndicator sharedInstance]showPIOnView:self withMessage:@"Uploading Images.."];
        NSString *name = [NSString stringWithFormat:@"%@_%d.png",[[NSUserDefaults standardUserDefaults] objectForKey:@"proid"],i];
        
        NSString *fullImageName = [NSString stringWithFormat:@"jobImages/%@",name];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyyMMddhhmmssa"];
        
        NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[formatter stringFromDate:[NSDate date]]]];
        
        
        //            NSURL *url = [NSURL URLWithString:_UpdatedImages[i]];
        //            NSData *data1 = [NSData dataWithContentsOfURL:url];
        //            UIImage *img = [[UIImage alloc] initWithData:data1];
        //            NSData *data = UIImageJPEGRepresentation(img,0.8);
        NSData *data = UIImageJPEGRepresentation(_cacheImages[i],0.8);
        
        [data writeToFile:getImagePath atomically:YES];
        
        [AmazonTransfer upload:getImagePath
                       fileKey:fullImageName
                      toBucket:Bucket
                      mimeType:@"image/jpeg"
               completionBlock:^(AWSS3TransferUtilityUploadTask *task, NSError *error){
                   
                   if (!error) {
                       NSLog(@"Uploaded Profile Image:%@",[NSString stringWithFormat:@"https://s3.amazonaws.com/%@/%@",Bucket,fullImageName]);
                   }else{
                       NSLog(@"Photo Upload Failed");
                   }
               }
         ];
    }
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [_jobCollection reloadData];
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(49,49);
}


@end
