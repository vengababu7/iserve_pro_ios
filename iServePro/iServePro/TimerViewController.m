//
//  TimerViewController.m
//  iServePro
//
//  Created by Rahul Sharma on 06/07/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "TimerViewController.h"
#import "CustomSliderView.h"
#import "InvoiceViewController.h"
#import "ChatSIOClient.h"
#import "AppointmentDetailController.h"
#import "BookingViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AmazonTransfer.h"
#import "TimerVCCollectionViewCell.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "ImageViewCollection.h"

@interface TimerViewController ()<CustomSliderViewDelegate,MFMessageComposeViewControllerDelegate>
{
    int timeSec, timeMin, timeHr;
    NSTimer *timerJobStarted;
    ImageViewCollection *collectionImages;
}
@property (strong, nonatomic) CustomSliderView *customSliderView;
@property(nonatomic,assign) NSInteger bookingStatu;
@property(nonatomic,assign) NSInteger hours;
@property(nonatomic,assign) NSInteger minutes;

@end

@implementation TimerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.noJobPhotos setHidden:YES];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    NSString *title =[NSString stringWithFormat:@"JOB ID:%@",_dictBookingDetails[@"bid"]];
    self.title =title;
    _pauseTimerLabel.layer.borderColor=UIColorFromRGB(0X2598ED).CGColor;
    _bookingStatu =[_dictBookingDetails[@"status"] integerValue];
    [self addCustomSlider];
    [self createNavLeftButton];
    [self updateCustomerinfo];
    
    NSDateFormatter *df = [self DateFormatterProper];
    
    NSDate *bookingDate = [df dateFromString:_dictBookingDetails[@"job_timer"]];
    
    NSTimeInterval timeDiff = [[NSDate date] timeIntervalSinceDate:bookingDate];
    
    
    if ([_dictBookingDetails[@"timer_status"]integerValue]==1) {
        
        timeHr=((int)timeDiff+[_dictBookingDetails[@"timer"] intValue])/3600;
        
        timeMin = fmod(((int)timeDiff+[_dictBookingDetails[@"timer"] intValue]), 3600)/60;
        timeSec = ((int)timeDiff+[_dictBookingDetails[@"timer"] intValue]) - ((timeHr*3600)+(timeMin*60));
        
        //        timeMin=((int)timeDiff+[_dictBookingDetails[@"timer"] intValue])%60;
        //        timeSec=((int)timeDiff+[_dictBookingDetails[@"timer"] intValue])%60;
        [self pauseTimer:nil];
    }else if ([_dictBookingDetails[@"timer_status"] integerValue]==2){
        
        timeHr=([_dictBookingDetails[@"timer"] intValue])/3600;
        timeMin = fmod(([_dictBookingDetails[@"timer"] intValue]), 3600)/60;
        timeSec = ([_dictBookingDetails[@"timer"] intValue]) - ((timeHr*3600)+(timeMin*60));
        
    }
    NSString* timeNow = [NSString stringWithFormat:@"%02d:%02d:%02d",timeHr, timeMin,timeSec];
    _timerLabel.text = timeNow;
}

- (NSDateFormatter *)DateFormatterProper {
    
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    });
    return formatter;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewDidDisappear:(BOOL)animated{
    [timerJobStarted invalidate];
}

-(void)addCustomSlider {
    
    _customSliderView = [[[NSBundle mainBundle] loadNibNamed:@"NewSliderView" owner:self options:nil] lastObject];
    _customSliderView.delegate = self;
    _customSliderView.sliderTitle.text = @"JOB STARTED";
    CGRect frame = _customSliderView.frame;
    frame.size.width = _sliderView.frame.size.width;
    frame.size.height = _sliderView.frame.size.height;
    _customSliderView.frame = frame;
    [_sliderView addSubview:_customSliderView];
    
}
-(void)updateCustomerinfo{
    _custName.text=_dictBookingDetails[@"fname"];
    if ([_dictBookingDetails[@"customer_notes"]  isEqual:@""]) {
        _jobDetails.text=@"No Notes Provided By The Customer";
        
    }else{
        _jobDetails.text=_dictBookingDetails[@"customer_notes"];
    }
    switch (_bookingStatu) {
        case 5:
            _customSliderView.sliderTitle.text  = @"JOB STARTED";
            break;
        case 21:
            _customSliderView.sliderTitle.text  = @"JOB STARTED";
            break;
        case 6:
            _customSliderView.sliderTitle.text  = @"JOB COMPLETED";
        default:
            break;
    }
}

-(void)createNavLeftButton
{
    UIImage *buttonImageNavNormal =[UIImage imageNamed:@"bnt_bck_normal.png"];
    UIImage *buttonImageNavHigh = [UIImage imageNamed:@"bnt_bck_pressed.png"];
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavHigh.size.width,buttonImageNavHigh.size.height)];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavNormal.size.width,buttonImageNavNormal.size.height)];
    [cancelButton setBackgroundImage:buttonImageNavHigh forState:UIControlStateHighlighted];
    [cancelButton setImage:buttonImageNavNormal forState:UIControlStateNormal];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc]
                                               initWithCustomView:cancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -5;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}


-(void)backButtonPressed {
    
    NSArray *vcs = self.navigationController.viewControllers;
    
    NSInteger count = [vcs count];
    
    if ([[vcs objectAtIndex:count-2] isKindOfClass:[BookingViewController class]]) {
        [self.navigationController popToViewController:[vcs objectAtIndex:0] animated:YES];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


#pragma mark - Custom Slider Delegate

-(void)sliderAction
{
    [self onTHEWAYButtonTapped];
}


-(void)onTHEWAYButtonTapped
{
    
    self.navigationItem.leftBarButtonItem.enabled = NO;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    
    switch (_bookingStatu) {
        case 5:{
            _bookingStatu = 6;
            
            break;
        }
        case 21:{
            _bookingStatu = 6;
            
            break;
        }
        case 6:{
            _bookingStatu = 22;
            break;
        }
        default:
            break;
    }
    NSDictionary *parameters;
    if (_bookingStatu==6) {
        parameters = @{
                       kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                       kSMPCommonDevideId       :[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                       @"ent_appnt_dt"          :self.dictBookingDetails[@"apntDt"],
                       KSMPPatientEmail         :self.dictBookingDetails[@"email"],
                       kSMPRespondResponse      :[NSString stringWithFormat:@"%ld",(long)_bookingStatu],
                       @"ent_date_time"         :[Helper getCurrentDateTime],
                       @"ent_bid"                :self.dictBookingDetails[@"bid"],
                       };
        
    }else{
        parameters = @{
                       kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                       kSMPCommonDevideId       :[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                       @"ent_appnt_dt"          :self.dictBookingDetails[@"apntDt"],
                       KSMPPatientEmail         :self.dictBookingDetails[@"email"],
                       kSMPRespondResponse      :[NSString stringWithFormat:@"%ld",(long)_bookingStatu],
                       @"ent_date_time"         :[Helper getCurrentDateTime],
                       @"ent_bid"                :self.dictBookingDetails[@"bid"],
                       @"ent_timer":[NSString stringWithFormat:@"%d",(timeHr*3600)+(timeMin*60)+timeSec],
                       };
    }
    
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    
    [handler composeRequestWithMethod:MethodupdateApptStatus
                              paramas:parameters
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             [[ProgressIndicator sharedInstance]hideProgressIndicator];
                             [_customSliderView sliderImageOrigin];
                             if (succeeded) {
                                 if ([response[@"errFlag"] isEqualToString:@"1"]) {
                                     [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                 }else{
                                     
                                     [self emitTheBookingACk:_bookingStatu];
                                     [self updateCustomerinfo];
                                     if (_bookingStatu==22) {
                                         [self performSegueWithIdentifier:@"toInvoiceSegue" sender:_dictBookingDetails];
                                     }else if (_bookingStatu==6){
                                         [self pauseTimer:nil];
                                     }
                                 }
                             }
                             else
                             {
                                 NSLog(@"Error");
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             }
                         }];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([[segue identifier] isEqualToString:@"toInvoiceSegue"])
    {
        InvoiceViewController *details =[segue destinationViewController];
        details.dictBookingDetails=sender;
        details.minutesCount=[NSString stringWithFormat:@"%d",(timeHr*3600)+(timeMin*60)+timeSec];
    }
    return;
}
-(void)emitTheBookingACk:(NSInteger)bstatus
{
    ChatSIOClient *socket = [ChatSIOClient sharedInstance];
    NSDictionary *message=@{
                            @"bid":_dictBookingDetails[@"bid"],
                            @"bstatus":[NSNumber numberWithInteger:bstatus],
                            @"cid":_dictBookingDetails[@"cid"],
                            @"proid":[[NSUserDefaults standardUserDefaults] objectForKey:@"ProviderId"],
                            @"dt":[Helper getCurrentDateTime]
                            };
    [socket publishToChannel:@"LiveBookingAck" message:message];
}

- (IBAction)pauseTimer:(id)sender {
    if (_bookingStatu == 5 ||_bookingStatu==21) {
        [Helper showAlertWithTitle:@"Message" Message:@"Please Start The Job"];
    }else{
        
        if(_pauseTimerLabel.selected)
        {
            _pauseTimerLabel.selected=NO;
            _timerTag=@"2";
            [self startTimer];
            
            
        }else{
            _pauseTimerLabel.selected=YES;
            _timerTag=@"1";
            [self startTimer];
            [self getTimerAPI];
        }
    }
    
}
- (IBAction)messageAction:(id)sender {
    if ([MFMessageComposeViewController canSendText]) {
        MFMessageComposeViewController *message = [[MFMessageComposeViewController alloc] init];
        message.messageComposeDelegate = self;
        
        message.recipients=@[_dictBookingDetails[@"phone"]];
        [[message navigationBar] setTintColor:[UIColor blackColor]];
        message.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
        
        [message setBody:@""];
        [self presentViewController:message animated:YES completion:nil];
    }
    
}

- (IBAction)callAction:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Call", @"Call") message:[NSString stringWithFormat:@"%@ ",_dictBookingDetails[@"phone"]] delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") otherButtonTitles:NSLocalizedString(@"Call", @"Call"), nil];
    [alert show];
}
-(void)alertView:(UIAlertView *)alertVie didDismissWithButtonIndex:(NSInteger) buttonIndex
{
    if (buttonIndex == 0)
    {
        NSLog(@"Cancel Tapped.");
    }
    else if (buttonIndex == 1)
    {
        NSString *phoneNumber = _dictBookingDetails[@"phone"];
        NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", phoneNumber];
        NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
}

-(void)startTimer {
    
    if (_pauseTimerLabel.selected) {
        timerJobStarted = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
    }else{
        NSString* timeNow = [NSString stringWithFormat:@"%02d:%02d:%02d",timeHr, timeMin,timeSec];
        _timerLabel.text = timeNow;
        [timerJobStarted invalidate];
        [self getTimerAPI];
    }
}


- (void)timerTick:(NSTimer *)timer
{
    
    timeSec++;
    if (timeSec == 60)
    {
        timeSec = 0;
        timeMin++;
    }
    if (timeMin == 60) {
        timeMin = 0;
        timeHr++;
    }
    
    NSString* timeNow = [NSString stringWithFormat:@"%02d:%02d:%02d",timeHr, timeMin,timeSec];
    _timerLabel.text = timeNow;
    
}


-(void)getTimerAPI{
    
    NSDictionary *dict =@{
                          @"ent_sess_token":[[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                          @"ent_dev_id":[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                          @"ent_bid":flStrForObj(_dictBookingDetails[@"bid"]),
                          @"ent_timer":[NSString stringWithFormat:@"%d",(timeHr*3600)+(timeMin*60)+timeSec],
                          @"ent_date_time":[Helper getCurrentDateTime],
                          @"ent_timer_status":flStrForObj(_timerTag)
                          };
    NetworkHandler *handler =[NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"UpdateApptTimer"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 //                                 [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                 NSLog(@"Timer updated:%@",response);
                                 
                                 if ([_timerTag isEqual:@"1"]) {
                                     [self updateToServe:@"15"];
                                 }else if ([_timerTag isEqual:@"2"]){
                                     [self updateToServe:@"16"];
                                 }
                             }else{
                                 
                             }
                         }];
}


-(void)updateToServe:(NSString *)status{
    
    ChatSIOClient *socket = [ChatSIOClient sharedInstance];
    NSDictionary *message=@{
                            @"bid":_dictBookingDetails[@"bid"],
                            @"bstatus":status,
                            @"cid":_dictBookingDetails[@"cid"],
                            @"proid":[[NSUserDefaults standardUserDefaults] objectForKey:@"ProviderId"],
                            @"dt":[Helper getCurrentDateTime]
                            };
    [socket publishToChannel:@"LiveBookingAck" message:message];
}
#pragma uicollectionView delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([_dictBookingDetails[@"job_imgs"] integerValue]==0) {
        [self.noJobPhotos setHidden:NO];
    }
    return [_dictBookingDetails[@"job_imgs"] integerValue];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier =@"customerImages";
    TimerVCCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSString *strImageUrl = [NSString stringWithFormat:@"https://s3.amazonaws.com/%@/JobImages/%@_%ld.png",Bucket,_dictBookingDetails[@"bid"],(long)indexPath.row];
    
    [cell.activityIndicator startAnimating];
    
    [cell.custImages sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                       placeholderImage:[UIImage imageNamed:@"user_image_default"]
                              completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                  [cell.activityIndicator stopAnimating];
                              }];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    collectionImages= [ImageViewCollection sharedInstance];
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [[NSUserDefaults standardUserDefaults] setObject:_dictBookingDetails[@"bid"] forKey:@"BID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSInteger profileTag =0;
    [collectionImages showPopUpWithDictionary:window jobImages:[_dictBookingDetails[@"job_imgs"] integerValue] index:indexPath tag:profileTag];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(66, 66);
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    switch (result) {
        case MessageComposeResultCancelled:
            NSLog(@"You cancelled sending message");
            break;
        case MessageComposeResultFailed:
            NSLog(@"Message failed");
            break;
        case MessageComposeResultSent:
            NSLog(@"Message sent");
            break;
            
        default:
            NSLog(@"An error occurred while composing this message");
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
