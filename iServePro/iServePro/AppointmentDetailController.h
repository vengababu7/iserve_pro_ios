//
//  AppointmentDetailController.h
//  iServePro
//
//  Created by Rahul Sharma on 03/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppointmentDetailController : UIViewController

@property(strong, nonatomic) NSMutableDictionary *dictAppointmentDetails;
@property (strong, nonatomic) IBOutlet UIView *sliderOuterView;
@property (strong, nonatomic) IBOutlet UIView *sliderView;
@property (strong, nonatomic) IBOutlet UIView *topView;

- (IBAction)callAction:(id)sender;
- (IBAction)messageAction:(id)sender;
- (IBAction)locateAction:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *custName;
@property (strong, nonatomic) IBOutlet UILabel *address1;
@property (strong, nonatomic) IBOutlet UILabel *address2;
@property (strong, nonatomic) IBOutlet UILabel *additionalNotes;
@property (strong, nonatomic) IBOutlet UILabel *noJobPhotos;

@end
