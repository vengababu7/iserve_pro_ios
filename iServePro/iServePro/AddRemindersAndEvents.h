//
//  AddRemindersAndEvents.h
//  IServePro
//
//  Created by Rahul Sharma on 29/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@import EventKit;

@interface AddRemindersAndEvents : NSObject

/**
 *  @brief it will give you the instance of AddRemindersAndEvents.
 *
 *  @return object of AddRemindersAndEvents.
 */
+(instancetype)instance;

/**
 *  @brief used for Authorization status to access event store if it is already accepted then it will add the event or else it will ask for the permission here.
 */
- (void)updateAuthorizationStatusToAccessEventStore;


//used for setting the start date of the event
@property (strong,nonatomic) NSDate *startingDate;

//used for setting the end date of the event
@property (strong,nonatomic) NSDate *endingDate;


-(EKEventStore *)eventStore;

@end
