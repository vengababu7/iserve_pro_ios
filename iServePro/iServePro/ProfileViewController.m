//
//  ProfileViewController.m
//  iServePro
//
//  Created by Rahul Sharma on 28/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "ProfileViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "CustomNavigationBar.h"
#import "User.h"
#import "LocationTracker.h"
#import "profileTableViewCell.h"
#import "iServeSplashController.h"
#import "ProfileCollectionViewCell.h"
#import "profileTableViewCell.h"
#import "collectionTableViewCell.h"
#import "ReviewTableViewCell.h"
#import "ChatSocketIOClient.h"

@interface ProfileViewController () <CustomNavigationBarDelegate,UITableViewDataSource,UITableViewDelegate, UserDelegate,UIImagePickerControllerDelegate>
{
    CGRect screenSize;
    UIImagePickerController *imagePicker;
    UIImage *pickedImage;
    NSMutableArray *jobImagesArray;
    collectionTableViewCell *jobPhotosCell;
    ChatSocketIOClient *socket;
}
@property(nonatomic,weak) IBOutlet UITableView *tableView;
@property(nonatomic,strong) UIImageView *profileImage;
@property(nonatomic,assign)BOOL isEditingModeOn;
@property(nonatomic,strong) NSArray *categories;
@property(nonatomic,strong) NSArray *reviewsArray;


@end




@implementation ProfileViewController



#pragma mark - ViewController LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.ratingView.highlightColor = UIColorFromRGB(0xffd200);
    self.ratingView.baseColor = UIColorFromRGB(0xcccccc);
    screenSize = [[UIScreen mainScreen]bounds];
    self.ratingView.markFont = [UIFont systemFontOfSize:18];
    _logoutButton.layer.borderWidth = 2;
    _logoutButton.layer.borderColor= [UIColorFromRGB(0x2598ED) CGColor];
}


-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    socket =[ChatSocketIOClient sharedInstance];
    jobImagesArray = [[NSMutableArray alloc]init];
    [self getProfileData];
}
-(void)viewDidAppear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accountDeactivated) name:@"accountDeactivated" object:nil];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)getProfileData
{
    
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:@"Loading.."];
    NSDictionary *queryParams;
    queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                   [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                   [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                   [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
    
    
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:MethodGetMasterProfile
                              paramas:queryParams
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             
                             if (succeeded) {
                                 
                                 [self getProfileResponse:response];
                             }
                         }];
    TELogInfo(@"param%@",queryParams);
}


-(void)getProfileResponse:(NSDictionary *)response      // errNum=21 for getting profile data.
{
    _profileArray=[[NSMutableArray alloc]init];
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    if ([response[@"errFlag"] intValue] ==0) {
        _categories = [response[@"selCat"] componentsSeparatedByString:@","];
        _reviewsArray =response[@"reviewsArr"];
        _noOfReviews.text=[NSString stringWithFormat:@"%lu Reviews",(unsigned long)_reviewsArray.count];
        [_profileArray addObject:response[@"about"]];
        [_profileArray addObject:response[@"expertise"]];
        [_profileArray addObject:response[@"languages"]];
        [_profileArray addObject:response[@"email"]];
        [_profileArray addObject:response[@"num_of_job_images"]];
        [self.tableView reloadData];
        
        [_ratingView setUserInteractionEnabled:NO];
        _ratingView.value=[response[@"avgRate"] floatValue];
        _proName.text=[NSString stringWithFormat:@"%@ %@",response[@"fName"],response[@"lName"]];
        NSString *strImageUrl = [NSString stringWithFormat:@"https://s3.amazonaws.com/iserve/ProfileImages/%@.jpg",response[@"email"]];
        
        [_profileImgView sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                           placeholderImage:[UIImage imageNamed:@"my_profile_profile_default_image.png"]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                  }];
    }
    else if ([response[@"errFlag"] intValue] == 1)
    {
        if ([response[@"errNum"] integerValue] == 7) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
            User *logout = [User sharedInstance];
            logout.delegate = self;
            [logout logout];
        }
        else if ([response[@"errNum"] integerValue] == 96 || [response[@"errNum"] integerValue] == 94) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
            User *logout = [User sharedInstance];
            logout.delegate = self;
            [logout logout];
        }
        else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
        }
    }
    else
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

/*-------------------------*/
#pragma mark - UserDelegate
/*-------------------------*/
-(void)userDidLogoutSucessfully:(BOOL)sucess {
    if (sucess){
        // Logged it out Successfully
        NSLog(@"Logged it out Successfully");
    }
    else{
        // Session is Expired
        NSLog(@"Session is Expired");
    }
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [[LocationTracker sharedInstance] stopLocationTracking];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    
    iServeSplashController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    
    self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
}

-(void)userDidFailedToLogout:(NSError *)error
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
}

-(void)accountDeactivated {
    [socket disconnectSocket];
    User *user = [User sharedInstance];
    user.delegate = self;
    [user logout];
}

- (IBAction)addPhotosButton:(id)sender {
    [self.view endEditing:YES];
    
    UIActionSheet *actionSheet;
    actionSheet.tag = 1;
    
    actionSheet = [[UIActionSheet alloc] initWithTitle:@"Add Job Photos"
                                              delegate:self
                                     cancelButtonTitle:@"Cancel"
                                destructiveButtonTitle:nil
                                     otherButtonTitles:@"Take Photo",@"Choose From Library",nil];
    
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];
    
}

- (IBAction)logoutAction:(id)sender {
    
    [self logoutMethod];
}
-(void)logoutMethod
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Confirm", @"Confirm")
                                                        message:NSLocalizedString(@"Are you sure you want to logout?", @"Are you sure you want to logout?")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"No", @"No")
                                              otherButtonTitles:NSLocalizedString(@"Yes", @"Yes"), nil];
    [alertView show];
}

/*-------------------------------*/
#pragma mark - UIAlertViewDelegate
/*-------------------------------*/
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        // logout
        dispatch_async(dispatch_get_main_queue(), ^{
            [socket disconnectSocket];
        });
        
        
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi showPIOnView:self.view
             withMessage:NSLocalizedString(@"Logging out..", @"Logging out..")];
        
        User *user = [[User alloc] init];
        user.delegate = self;
        [user logout];
    }
}

#pragma mark UITableView Delegate -

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    
    switch (indexPath.section)
    {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        {
            cellIdentifier = @"categories";
            profileTableViewCell    *detailsCell = (profileTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if(detailsCell == nil)
            {
                detailsCell =[[profileTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            
            if(indexPath.section == 0)
            {
                detailsCell.detailLabel.text = _categories[indexPath.row];
            }
            else if(indexPath.section == 1)
            {
                detailsCell.detailLabel.text = _profileArray[indexPath.section-1];
            }
            else if(indexPath.section == 2)
            {
                detailsCell.detailLabel.text = _profileArray[indexPath.section-1];
            }
            else if(indexPath.section == 3)
            {
                detailsCell.detailLabel.text = _profileArray[indexPath.section-1];
            }
            else
            {
                detailsCell.detailLabel.text = _profileArray[indexPath.section-1];
            }
            
            float height = [self measureHeightLabel:detailsCell.detailLabel];
            return 16+height;
        }
            break;
            
        case 5:
        {
            return 70;
        }
        case 6:
        {
            cellIdentifier = @"reviews";
            ReviewTableViewCell *reviewcell = (ReviewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if(reviewcell == nil)
            {
                reviewcell =[[ReviewTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            
            reviewcell.review.text=_reviewsArray[indexPath.row][@"review"];
            float height = [self measureHeiLabel:reviewcell.review];
            return 46+height;
        }
        default:
            return 0;
            break;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

#pragma mark UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 6;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    if(section == 6)
    //    {
    //        return _reviewsArray.count;
    //    }else
    if (section==0)
    {
        return _categories.count;
    }
    else
    {
        return 1;
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    
    switch (indexPath.section)
    {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        {
            cellIdentifier = @"categories";
            profileTableViewCell *detailsCell= (profileTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if(detailsCell == nil)
            {
                detailsCell =[[profileTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            if(indexPath.section == 0)
            {
                detailsCell.detailLabel.text =  _categories[indexPath.row];
            }
            else if(indexPath.section == 1)
            {
                detailsCell.detailLabel.text = _profileArray[indexPath.section-1];
            }
            else if(indexPath.section == 2)
            {
                detailsCell.detailLabel.text = _profileArray[indexPath.section-1];
            }
            else if(indexPath.section == 3)
            {
                detailsCell.detailLabel.text = _profileArray[indexPath.section-1];
            }
            else
            {
                detailsCell.detailLabel.text =_profileArray[indexPath.section-1];
            }
            detailsCell.selectionStyle=UITableViewCellSelectionStyleNone;
            
            return detailsCell;
        }
            break;
            
        default:
        {
            cellIdentifier = @"jobimages";
            jobPhotosCell = (collectionTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if(jobPhotosCell == nil)
            {
                jobPhotosCell =[[collectionTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            jobPhotosCell.UpdatedImages=[[NSMutableArray alloc]init];
            [jobPhotosCell reloadCollection:[_profileArray[indexPath.section-1] integerValue]];
            jobPhotosCell.selectionStyle=UITableViewCellSelectionStyleNone;
            return jobPhotosCell;
        }
            break;
            
            //        default:
            //        {
            //            cellIdentifier = @"reviews";
            //            ReviewTableViewCell *reviewcell = (ReviewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            //
            //            if(reviewcell == nil)
            //            {
            //                reviewcell =[[ReviewTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            //            }
            //            reviewcell.custName.text=[NSString stringWithFormat:@"%@ %@",_reviewsArray[indexPath.row][@"fname"],_reviewsArray[indexPath.row][@"lname"]];
            //            reviewcell.review.text=_reviewsArray[indexPath.row][@"review"];
            //            [reviewcell.profileImages sd_setImageWithURL:[NSURL URLWithString:_reviewsArray[indexPath.row][@"img"]]
            //                                        placeholderImage:[UIImage imageNamed:@"user_image_default"]
            //                                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
            //
            //
            //                                               }];
            //
            //            reviewcell.selectionStyle=UITableViewCellSelectionStyleNone;
            //            return reviewcell;
            //
            //        }
            //            break;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma mark - UITableViewDataSource Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    UIView * labelview  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 480, 26)];
    UILabel *labelHeader = [[UILabel alloc] initWithFrame:CGRectMake(18, 3, 480, 22)];
    UIView * divider  = [[UIView alloc]initWithFrame:CGRectMake(18, 25, 480, 1)];
    labelview.backgroundColor=UIColorFromRGB(0XFFFFFF);
    divider.backgroundColor=UIColorFromRGB(0XEEEEEE);
    labelHeader.minimumScaleFactor=0.5;
    [labelview addSubview:labelHeader];
    [labelview addSubview:divider];
    
    if (section == 0) {
        [Helper setToLabel:labelHeader Text:@"APPROVEd CATEGORIES" WithFont:@"opensans-semibold" FSize:10 Color:UIColorFromRGB(0XCCCCCC)];
    }
    else if (section == 1){
        
        [Helper setToLabel:labelHeader Text:@"ABOUT ME" WithFont:@"opensans-semibold" FSize:10 Color:UIColorFromRGB(0XCCCCCC)];
    }
    else if (section == 2){
        
        [Helper setToLabel:labelHeader Text:@"AREA OF EXPERTISE" WithFont:@"opensans-semibold" FSize:10 Color:UIColorFromRGB(0XCCCCCC)];
    }
    else if (section == 3){
        
        [Helper setToLabel:labelHeader Text:@"LANGUAGE" WithFont:@"opensans-semibold" FSize:10 Color:UIColorFromRGB(0XCCCCCC)];
    }
    else if (section == 4){
        
        [Helper setToLabel:labelHeader Text:@"EMAIL ID" WithFont:@"opensans-semibold" FSize:10 Color:UIColorFromRGB(0XCCCCCC)];
    }
    
    else if (section == 5){
        _addButton = [[UIButton alloc] initWithFrame:CGRectMake(250, 3, 60, 22)];
        [Helper setButton:_addButton Text:@"ADD IMAGES" WithFont:@"opensans" FSize:10 TitleColor:UIColorFromRGB(0XCCCCCC) ShadowColor:UIColorFromRGB(0XCCCCCC)];
        [Helper setToLabel:labelHeader Text:@"JOB PHOTOS" WithFont:@"opensans-semibold" FSize:10 Color:UIColorFromRGB(0XCCCCCC)];
        [_addButton addTarget:self action:@selector(sectionTapped) forControlEvents:UIControlEventTouchDown];
        [labelview addSubview:_addButton];
    }
    //    else if (section == 6){
    //
    //        [Helper setToLabel:labelHeader Text:@"REVIEWS" WithFont:@"opensans" FSize:9 Color:UIColorFromRGB(0XCCCCCC)];
    //    }
    
    return labelview;
}
-(void)sectionTapped{
    if ([_addButton.titleLabel.text isEqual:@"ADD IMAGES"]) {
        [Helper setButton:_addButton Text:@"DONE" WithFont:@"opensans" FSize:10 TitleColor:UIColorFromRGB(0XCCCCCC) ShadowColor:UIColorFromRGB(0XCCCCCC)];
        [jobImagesArray removeAllObjects];
    }else{
        [Helper setButton:_addButton Text:@"ADD IMAGES" WithFont:@"opensans" FSize:10 TitleColor:UIColorFromRGB(0XCCCCCC) ShadowColor:UIColorFromRGB(0XCCCCCC)];
    }
    [jobPhotosCell addImagesSelected];
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *view;
    
    //    if(section == 4){
    //
    //        if([providerDetails[section] count] == 0)
    //        {
    //            //If No Reviews
    //            view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width,50)];
    //            view.backgroundColor = UIColorFromRGB(0xffffff);
    //            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, view.frame.size.width,50)];
    //            label.text = @"No Reviews";
    //            label.textAlignment = NSTextAlignmentCenter;
    //            label.font = [UIFont fontWithName:@"Opensans" size:15];
    //            label.textColor = UIColorFromRGB(0x333333);
    //
    //            [view addSubview:label];
    //            return view;
    //        }
    //    }
    
    view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width,5)];
    view.backgroundColor = UIColorFromRGB(0xEDEDED);
    
    return view;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    //    if(section == sectionTitleArray.count-1){
    //
    //        if([providerDetails[section] count] == 0)
    //        {
    //            return 50;
    //        }
    //        return 1;
    //    }
    //    return 5;
    return 5;
}

#pragma mark - Custom Methods -

- (CGFloat)measureHeightLabel: (UILabel *)label
{
    CGSize constrainedSize = CGSizeMake(screenSize.size.width-26 , 9999);
    
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:label.font.fontName size:label.font.pointSize], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:flStrForObj(label.text)attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return  requiredHeight.size.height;
}

#pragma mark - Custom Methods -

- (CGFloat)measureHeiLabel: (UILabel *)label
{
    CGSize constrainedSize = CGSizeMake(screenSize.size.width-80 , 9999);
    
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:label.font.fontName size:label.font.pointSize], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:flStrForObj(label.text)attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return  requiredHeight.size.height;
}


#pragma mark - UIImagePickerDelegate -

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch (buttonIndex)
    {
        case 0:
        {
            [self cameraButtonClicked];
            break;
        }
        case 1:
        {
            [self libraryButtonClicked];
            break;
        }
        default:
            break;
    }
}


-(void)cameraButtonClicked
{
    imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate = self;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message: @"Camera is not available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
}

-(void)libraryButtonClicked
{
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    imagePicker.allowsEditing = YES;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
    } else {
        
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    pickedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    
    pickedImage = [self imageWithImage:pickedImage scaledToSize:CGSizeMake(300,300)];
    [jobImagesArray addObject:pickedImage];
    [jobPhotosCell reloadCollectionView:jobImagesArray];
    
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
@end
