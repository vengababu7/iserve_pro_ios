//
//  AppDelegate.m
//  iServePro
//
//  Created by Rahul Sharma on 24/02/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "IServeAppDelegate.h"
#import "iServeHelpController.h"
#import "iServeSplashController.h"
#import "PMDReachabilityWrapper.h"
#import "IServeHomeViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "NewBookingController.h"
#import "ChatSIOClient.h"
#import "ChatSocketIOClient.h"
#import "HomeTabBarController.h"
#import "AmazonTransfer.h"
#import "NewBookingController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>



@interface IServeAppDelegate ()
{
    NSString *alertMsg;
    ChatSocketIOClient *client;
    UIBackgroundTaskIdentifier bgTask;
}
@property (nonatomic,strong) iServeHelpController *helpViewController;
@property (nonatomic,strong) iServeSplashController *splashViewController;
//@property (nonatomic,strong) MapViewController *mapViewContoller;

@property (nonatomic, strong) Reachability *hostReach;
@property (nonatomic, strong) Reachability *internetReach;
@property (nonatomic, strong) Reachability *wifiReach;
@property (nonatomic, strong) NSDictionary *pushInfo;
@property (strong, nonatomic) UIStoryboard *mainstoryboard;
@property (nonatomic) NSTimer* locationUpdateTimer;


@end

@implementation IServeAppDelegate
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize hostReach;
@synthesize internetReach;
@synthesize wifiReach;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    bgTask=0;
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    [self registerForNotifications:application];
    //LumberJack init
    
    [[TELogger getInstance] initiateFileLogging];
    
    
    [GMSServices provideAPIKey:kPMDGoogleMapsAPIKey];
    [AmazonTransfer setConfigurationWithRegion:AWSRegionUSEast1 accessKey:AmazonAccessKey secretKey:AmazonSecretKey];
    //  [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [self handlePushNotificationWithLaunchOption:launchOptions];
    [self insertDefaultValue];
    [self setupAppearance];
    [Fabric with:@[[Crashlytics class]]];
    
    return YES;
}


-(void)registerForNotifications:(UIApplication *)application
{
    //Configuring for Interactive Notifications
    UIMutableUserNotificationAction *accept = [[UIMutableUserNotificationAction alloc] init];
    accept.identifier = @"Accept";
    accept.title = @"Accept";
    accept.activationMode = UIUserNotificationActivationModeForeground;
    accept.destructive = NO;
    accept.authenticationRequired = YES;
    
    UIMutableUserNotificationAction *reject = [[UIMutableUserNotificationAction alloc] init];
    reject.identifier = @"Reject";
    reject.title = @"Reject";
    reject.activationMode = UIUserNotificationActivationModeBackground;
    reject.destructive = NO;
    reject.authenticationRequired = YES;
    
    UIMutableUserNotificationCategory *notificationCategory = [[UIMutableUserNotificationCategory alloc] init];
    notificationCategory.identifier = @"JobRequest";
    [notificationCategory setActions:@[accept,reject] forContext:UIUserNotificationActionContextDefault];
    
    NSSet *categories = [NSSet setWithObjects:notificationCategory, nil];
    
    
    
    //Registering for Local Notification
    [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeSound categories:categories]];
    
    //Registering for Remote Notification
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert ) categories:categories]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    // from  socket
    
    NSDictionary *dict = notification.userInfo;
    NSArray *bookingArray = dict[@"aps"];
    NSInteger btype = [bookingArray[0][@"btype"]integerValue];
    if (btype == 1 || btype == 3) {
        [self openNewBookingVCWithInfo:dict[@"aps"]];
    }else if(btype == 2){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissNewBookingVC" object:nil];
        UINavigationController *naviVC =(UINavigationController*) self.window.rootViewController;
        
        TELogInfo(@"currentViewControll : %@", naviVC.viewControllers);
        
        if ([[naviVC.viewControllers lastObject] isKindOfClass:[IServeHomeViewController class]]) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissNewBookingVC" object:nil];
        }
        [Helper showAlertWithTitle:@"Message" Message:[NSString stringWithFormat:@"Congrats you got scheduling booking from%@ ", bookingArray[0][@"cname"]]];
    }
}


- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    
    NSString *dt = [[deviceToken description] stringByTrimmingCharactersInSet:
                    [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    dt = [dt stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"push token distribution: %@", dt);
    
    NSString *model = [[UIDevice currentDevice] model];
    if ([model isEqualToString:@"iPhone Simulator"]) {
        //device is simulator
        [[NSUserDefaults standardUserDefaults]setObject:@"123" forKey:KDAgetPushToken];
    }
    else {
        [[NSUserDefaults standardUserDefaults]setObject:dt forKey:KDAgetPushToken];
    }
    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
    NSString *model = [[UIDevice currentDevice] model];
    if ([model isEqualToString:@"iPhone Simulator"])
    {
        //device is simulator
        [[NSUserDefaults standardUserDefaults]setObject:@"123" forKey:KDAgetPushToken];
    }
    
}


- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void(^)())completionHandler {
    
    NSDictionary *dict = [notification.userInfo mutableCopy];
    NewBookingController *new =[[NewBookingController alloc]init];
    if([identifier isEqualToString:@"Accept"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"2" forKey:@"bookingStatusResponse"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [new receiveNotification:dict];
    }
    else if([identifier isEqualToString:@"Reject"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"3" forKey:@"bookingStatusResponse"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [new receiveNotification:dict];
    }
    if(completionHandler != nil)    //Finally call completion handler if its not nil
        completionHandler();
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    ChatSocketIOClient *socket =[ChatSocketIOClient sharedInstance];
    
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"LoggedIn"])
    {
        //and create new timer with async call:
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            //run function methodRunAfterBackground
            [socket heartBeat:@"0"];
            NSLog(@"Become in Background=Called");
        });
    }
    NSLog(@"Become in Background");
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {


    if([[NSUserDefaults standardUserDefaults] boolForKey:@"loggedIN"])
    {
        ChatSocketIOClient *socket=[ChatSocketIOClient sharedInstance];
        
        [socket heartBeat:@"1"];
    }
    NSLog(@"Become in Foreground");
    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    PMDReachabilityWrapper *networkHandler = [PMDReachabilityWrapper sharedInstance];
    [networkHandler monitorReachability];
    if([application backgroundRefreshStatus] == UIBackgroundRefreshStatusDenied)
    {
        NSLog(@"Background Denied");
    }
    else if([application backgroundRefreshStatus] == UIBackgroundRefreshStatusRestricted)
    {
        NSLog(@"Background Restricted");
    }
    else
    {
        NSLog(@"Background Enable");
    }
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"LoggedIn"])
    {
        client =[ChatSocketIOClient sharedInstance];
    }
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"user Info %@",userInfo);
    [self handleNotificationForUserInfo:userInfo];
    
}

#pragma mark -
#pragma mark Core Data stack
// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}



- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    
    return YES;
}


void uncaughtExceptionHandler(NSException *exception)
{
    NSLog(@"The app has encountered an unhandled exception: %@", [exception debugDescription]);
    NSLog(@"Stack trace: %@", [exception callStackSymbols]);
    NSLog(@"desc: %@", [exception description]);
    NSLog(@"name: %@", [exception name]);
    NSLog(@"user info: %@", [exception userInfo]);
}

- (void)insertDefaultValue{
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    if ([ud boolForKey:@"isFirstLoginCompleted"] == NO) {
        
        [ud setBool:YES forKey:@"isFirstLoginCompleted"];
        
        [ud setValue:@"3" forKey:@"DoctorStatus"];
    }
}

#pragma mark - HandlePushNotification
/**
 *  handle push if app is opened by clicking push
 *
 *  @param launchOptions pushpayload dictionary
 */
-(void)handlePushNotificationWithLaunchOption:(NSDictionary*)launchOptions
{
    NSDictionary *remoteNotificationPayload = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (remoteNotificationPayload)
    {
        //check if user is logged in
        if([[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken])
        {
            [self handleNotificationForUserInfo:remoteNotificationPayload];
        }
    }
}



#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma Reachability

- (BOOL)isNetworkAvailable {
    
    return self.networkStatus != NotReachable;
}

// Called by Reachability whenever status changes.
- (void)reachabilityChanged:(NSNotification* )note {
    
    Reachability *curReach = (Reachability *)[note object];
    
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    
    _networkStatus = [curReach currentReachabilityStatus];
    
    if (_networkStatus == NotReachable) {
        NSLog(@"Network not reachable.");
    }
    
}
- (void)monitorReachability {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    self.hostReach = [Reachability reachabilityWithHostName:@"http://www.IServe.com"];
    [self.hostReach startNotifier];
    
    self.internetReach = [Reachability reachabilityForInternetConnection];
    [self.internetReach startNotifier];
    
    self.wifiReach = [Reachability reachabilityForLocalWiFi];
    [self.wifiReach startNotifier];
}
- (void)saveContext
{
    
}

/*----------------------------------------*/
#pragma mark - Set Up NAvigation Appearance
/*----------------------------------------*/
- (void)setupAppearance
{
    [[UINavigationBar appearance] setBackgroundColor:UIColorFromRGB(0x041420)];
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"login_navigation_bar.png"] forBarMetrics:UIBarMetricsDefault];
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"opensans" size:17], NSFontAttributeName,UIColorFromRGB(0x2598ED), NSForegroundColorAttributeName, nil];
    [[UINavigationBar appearance] setTitleTextAttributes:textAttributes];
}

-(void)checkDriverStatus{
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kNSURoadyoPubNubChannelkey])
    {
        ChatSIOClient *socket =[ChatSIOClient sharedInstance];
        
        NSString *driverEmail = [[NSUserDefaults standardUserDefaults] objectForKey:@"dEmail"];
        NSString *deviceUDID = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
        
        NSDictionary *message = @{@"a":[NSNumber numberWithInt:2],
                                  @"e_id": driverEmail,
                                  @"d_id":deviceUDID,
                                  };
        [socket publishToChannel:kISPSocketChannel message:message];
    }
}




- (void)handleNotificationForUserInfo:(NSDictionary*)userInfo{
    
    NSLog(@"handle push %@",userInfo);
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef soundFileURLRef = CFBundleCopyResourceURL(mainBundle, CFSTR("sms-received"), CFSTR("wav"), NULL);
    SystemSoundID soundId;
    AudioServicesCreateSystemSoundID(soundFileURLRef, &soundId);
    AudioServicesPlaySystemSound(soundId);
    CFRelease(soundFileURLRef);
    
    
    [[NSUserDefaults standardUserDefaults] setObject:userInfo forKey:@"PUSH"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isBooked"];
    
    self.pushInfo = userInfo;
    
    int type = [userInfo[@"aps"][@"nt"] intValue];
    alertMsg = userInfo[@"aps"][@"alert"];
    switch (type) {
        case 1:  //live Booking
        {
            [self openNewBookingVCWithInfo:self.pushInfo];
        }
            break;
        case 2: // later booking
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissNewBookingVC" object:nil];
            UINavigationController *naviVC =(UINavigationController*) self.window.rootViewController;
            
            TELogInfo(@"currentViewControll : %@", naviVC.viewControllers);
            
            if ([[naviVC.viewControllers lastObject] isKindOfClass:[IServeHomeViewController class]]) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissNewBookingVC" object:nil];
            }
            
        }
            break;
        case 3:// cancel booking
        {
            UINavigationController *naviVC =(UINavigationController*) self.window.rootViewController;
            
            TELogInfo(@"currentViewControll : %@", naviVC.viewControllers);
            
            if ([[naviVC.viewControllers lastObject] isKindOfClass:[IServeHomeViewController class]]) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"AppointmentCancel" object:nil userInfo:nil];
            }
            
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:alertMsg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            [naviVC popToRootViewControllerAnimated:YES];
            
        }break;
            
        case 4: // accept from admin
        case 5: // reject from admin
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:alertMsg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            
            UINavigationController *naviVC =(UINavigationController*) self.window.rootViewController;
            if ([[[[[[naviVC.viewControllers objectAtIndex:0] childViewControllers] objectAtIndex:1]  childViewControllers] lastObject] isKindOfClass:[IServeHomeViewController class]]) {
                alert.tag = 400;
                [alert show];
            }
        }
            break;
            
            
        default:
            break;
    }
}



-(void)openNewBookingVCWithInfo:(NSDictionary *)info
{
    IServeAppDelegate *tmpDelegate = (IServeAppDelegate *)[[UIApplication sharedApplication] delegate];
    UIViewController *vc = ((UINavigationController*)tmpDelegate.window.rootViewController).visibleViewController;
    
    if (!_mainstoryboard) {
        _mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
    NewBookingController *newBookingVC = [_mainstoryboard instantiateViewControllerWithIdentifier:@"newBookingVC"];
    newBookingVC.detailsDict=info;
    newBookingVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [newBookingVC setModalPresentationStyle:UIModalPresentationOverFullScreen];
    [vc presentViewController:newBookingVC animated:YES completion:nil];
}


- (NSDateFormatter *)DateFormatterProper {
    
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    });
    return formatter;
}


@end
