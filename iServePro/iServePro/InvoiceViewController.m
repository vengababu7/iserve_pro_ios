//
//  InvoiceViewController.m
//  iServePro
//
//  Created by Rahul Sharma on 27/06/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "InvoiceViewController.h"
#import "AppointmentDetailController.h"
#import "AmazonTransfer.h"
#import "ChatSIOClient.h"
#import "InvoiceViewController.h"
#import "BookingViewController.h"
#import "TimerViewController.h"
#import "CustomSliderView.h"


@interface InvoiceViewController ()<CustomSliderViewDelegate>
@property (assign, nonatomic) BOOL isSignatureDone;
@property (strong, nonatomic) UIImage *signImage;
@property (strong, nonatomic) NSString *invoiceImageURL;
@property (nonatomic) float initialOffsetY;
@property (strong, nonatomic) CustomSliderView *customSliderView;





@end

@implementation InvoiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _invoiceImageURL = nil;
    [self addCustomSlider];
    
    _matFee=0;
    _miscFee=0;
    _proDisc=0;
    
    _retake.layer.borderColor=UIColorFromRGB(0Xc6c6c6).CGColor;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    NSString *title =[NSString stringWithFormat:@"JOB ID:%@",_dictBookingDetails[@"bid"]];
    self.title =title;
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    
    NSNumber *mateFee =[NSNumber numberWithInteger:[_materialFees.text integerValue]];
    NSString *totalMat = [formatter stringFromNumber:mateFee];
    _materialFees.text=totalMat;
    
    NSNumber *miscFee =[NSNumber numberWithInteger:[_miscFees.text integerValue]];
    NSString *totMisc = [formatter stringFromNumber:miscFee];
    _miscFees.text=totMisc;
    
    NSNumber *proFee =[NSNumber numberWithInteger:[_proDiscount.text integerValue]];
    NSString *totPro = [formatter stringFromNumber:proFee];
    _proDiscount.text=totPro;
    
    
    _signView.layer.masksToBounds = YES;
    _isSignatureDone = NO;
    _signView.delegate = self;
    [self createNavLeftButton];
    _invoiceImageURL =nil;
    if (_minutesCount) {
        _timeTakenForJob.text=[NSString stringWithFormat:@"TimeFee(%td hr %d mins)",[_minutesCount integerValue]/3600,(int)fmod(([_minutesCount integerValue]), 3600)/60 ];
    }else{
        _timeTakenForJob.text=[NSString stringWithFormat:@"TimeFee(%td hr %d mins)",[_dictBookingDetails[@"timer"] integerValue]/3600,(int)fmod(([_dictBookingDetails[@"timer"] integerValue]), 3600)/60 ];
        _minutesCount =_dictBookingDetails[@"timer"];
    }
    [self raiseInvoice];
}

-(void)addCustomSlider {
    
    _customSliderView = [[[NSBundle mainBundle] loadNibNamed:@"NewSliderView" owner:self options:nil] lastObject];
    _customSliderView.delegate = self;
    _customSliderView.sliderTitle.text = @"INVOICE CUSTOMER";
    CGRect frame = _customSliderView.frame;
    frame.size.width = _sliderView.frame.size.width;
    frame.size.height = _sliderView.frame.size.height;
    _customSliderView.frame = frame;
    [_sliderView addSubview:_customSliderView];
    
}
/**
 *  slider Origin
 *
 */
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if(alertView.tag==2)
    {
        [_customSliderView sliderImageOrigin];
    }
    
}


-(void)raiseInvoice{
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:@"Loading..."];
    NSDictionary *dict =@{
                          @"ent_minutes":[NSString stringWithFormat:@"%d",[_minutesCount integerValue]/60],
                          @"ent_bid":flStrForObj(_dictBookingDetails[@"bid"]),
                          @"ent_session_token":[[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                          @"ent_dev_id":[[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey],
                          @"ent_date_time":[Helper getCurrentDateTime]
                          
                          };
    NetworkHandler *handler =[NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"raiseInvoice"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 [self invoiceCalculation:response];
                             }
                         }];
}

-(void)invoiceCalculation:(NSDictionary*)dict
{
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    _invoiceDetails =[dict mutableCopy];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    
    NSNumber *value =[NSNumber numberWithInteger:[_invoiceDetails[@"totalAmt"] integerValue]];
    NSString *timeAmt = [formatter stringFromNumber:value];
    
    NSNumber *value1 =[NSNumber numberWithInteger:[_invoiceDetails[@"visitAmt"]  integerValue]];
    NSString *visitAmt = [formatter stringFromNumber:value1];
    
    _timeFare.text=timeAmt;
    _visitFare.text=visitAmt;
    
    
    NSNumber *discout =[NSNumber numberWithInteger:[_invoiceDetails[@"discount"] integerValue]];
    NSString *disc = [formatter stringFromNumber:discout];
    _discount.text=disc;
    
    NSNumber *Total =[NSNumber numberWithInteger:[flStrForObj(_invoiceDetails[@"visitAmt"])integerValue]+[flStrForObj(_miscFees.text) integerValue]+[flStrForObj(_materialFees.text) integerValue]-[flStrForObj(_proDiscount.text) integerValue]+[flStrForObj(_invoiceDetails[@"totalAmt"]) integerValue]-[flStrForObj(_invoiceDetails[@"discount"]) integerValue]];
    NSString *TotalAmt = [formatter stringFromNumber:Total];
    _totalAmt.text=TotalAmt;
    _mainTotalAmt=[Total integerValue];
    
    
    NSNumber *subTotAmt =[NSNumber numberWithInteger:[flStrForObj(_invoiceDetails[@"visitAmt"])integerValue]+[flStrForObj(_miscFees.text) integerValue]+[flStrForObj(_materialFees.text) integerValue]-[flStrForObj(_proDiscount.text) integerValue]+[flStrForObj(_invoiceDetails[@"totalAmt"]) integerValue]];
    NSString *subTot = [formatter stringFromNumber:subTotAmt];
    _subTotal.text=subTot;
    _subTotalAmt=[subTotAmt integerValue];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dismissKeyboard
{
    [_materialFees resignFirstResponder];
    [_miscFees resignFirstResponder];
    [_proDiscount resignFirstResponder];
    [self.view endEditing:YES];
    
}

-(void) createNavLeftButton
{
    UIImage *buttonImageNavNormal =[UIImage imageNamed:@"bnt_bck_normal.png"];
    UIImage *buttonImageNavHigh = [UIImage imageNamed:@"bnt_bck_pressed.png"];
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavHigh.size.width,buttonImageNavHigh.size.height)];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavNormal.size.width,buttonImageNavNormal.size.height)];
    [cancelButton setBackgroundImage:buttonImageNavHigh forState:UIControlStateHighlighted];
    [cancelButton setImage:buttonImageNavNormal forState:UIControlStateNormal];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc]
                                               initWithCustomView:cancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -5; //it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}


-(void)backButtonPressed{
    NSArray *vcs = self.navigationController.viewControllers;
    NSInteger count = [vcs count];
    if ([[vcs objectAtIndex:count-2] isKindOfClass:[TimerViewController class]]) {
        [self.navigationController popToViewController:[vcs objectAtIndex:0] animated:YES];
    }else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)signatureMade {
    _isSignatureDone = YES;
}

-(void)sliderAction {
    if (_proDisc>_miscFee+_matFee) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message",@"Message") message:NSLocalizedString(@"Provider Discount shouldn't be greater than material and miscellaneous fees",@"Provider Discount shouldn't be greater than material and miscellaneous fees") delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        alert.tag=2;
        [alert show];
    }else if (_isSignatureDone ) {
        _signImage = [self imageFromView: _signView];
        if (_invoiceImageURL) {
            [self updateAppointmentStatus];
            
        }else {
            CGRect rect = CGRectMake(0,0,150,150);
            UIGraphicsBeginImageContext( rect.size );
            [_signImage drawInRect:rect];
            UIImage *picture1 = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            NSData *imageData = UIImagePNGRepresentation(picture1);
            UIImage *img=[UIImage imageWithData:imageData];
            [self uploadImage:img];
        }
    }else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message",@"Message") message:NSLocalizedString(@"Please make a signature",@"Please make a signature") delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        alert.tag=2;
        [alert show];
    }
}


/**
 *  it takes view as image
 *
 *  @param view signature
 *
 *  @return image
 */

- (UIImage *)imageFromView:(UIView *) view
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
/**
 *  upload the image in amazon
 *
 *  @param sign image of received person
 */
-(void)uploadImage:(UIImage *)signImage
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    NSString *name = [NSString stringWithFormat:@"%@.jpg",[self getCurrentTimeDate]];
    
    NSString *fullImageName = [NSString stringWithFormat:@"invoice/%@",name];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddhhmmssa"];
    
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[formatter stringFromDate:[NSDate date]]]];
    
    NSData *data = UIImageJPEGRepresentation(signImage,0.8);
    [data writeToFile:getImagePath atomically:YES];
    
    
    [AmazonTransfer upload:getImagePath
                   fileKey:fullImageName
                  toBucket:Bucket
                  mimeType:@"image/jpeg"
           completionBlock:^(AWSS3TransferUtilityUploadTask *task, NSError *error){
               
               if (!error) {
                   NSLog(@"Uploaded Profile Image:%@",[NSString stringWithFormat:@"https://s3.amazonaws.com/%@/%@",Bucket,fullImageName]);
                   
                   _invoiceImageURL=[NSString stringWithFormat:@"https://s3.amazonaws.com/%@/%@",Bucket,fullImageName];
                   [self updateAppointmentStatus];
               }else{
                   [_customSliderView sliderImageOrigin];
                   NSLog(@"Photo Upload Failed");
               }
               
           }
     ];
    
}

-(void)updateAppointmentStatus{
    if ([_notes.text isEqual:@"Enter required notes"]) {
        dispatch_async(dispatch_get_main_queue(),^{
            _notes.text=@"No Notes";
        });
    }
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    NSDictionary *parameters = @{
                                 kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                                 kSMPCommonDevideId       :[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                                 @"ent_appnt_dt"          :self.dictBookingDetails[@"apntDt"],
                                 KSMPPatientEmail         :self.dictBookingDetails[@"email"],
                                 kSMPRespondResponse      :@"7",
                                 @"ent_date_time"         :[Helper getCurrentDateTime],
                                 @"ent_bid"               :self.dictBookingDetails[@"bid"],
                                 @"ent_signature_url"     :flStrForObj(_invoiceImageURL),
                                 @"ent_misc_fees"         :[NSString stringWithFormat:@"%d",(int)_miscFee] ,
                                 @"ent_mat_fees"          :[NSString stringWithFormat:@"%d",(int)_matFee],
                                 @"ent_pro_disc"          :[NSString stringWithFormat:@"%d",(int)_proDisc],
                                 @"ent_total_pro"         :[NSString stringWithFormat:@"%ld",(long)_mainTotalAmt],
                                 @"ent_notes"             :flStrForObj(_notes.text),
                                 @"ent_sub_total_pro":[NSString stringWithFormat:@"%d",_subTotalAmt],
                                 @"ent_time_fees":_invoiceDetails[@"totalAmt"]
                                 };
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    
    [handler composeRequestWithMethod:MethodupdateApptStatus
                              paramas:parameters
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             [[ProgressIndicator sharedInstance]hideProgressIndicator];
                             if (succeeded) {
                                 if ([response[@"errFlag"] isEqualToString:@"1"]) {
                                     [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                 }else{
                                     [self emitTheBookingACk:7];
                                     [self.navigationController popToRootViewControllerAnimated:YES];
                                 }
                             }
                             else
                             {
                                 NSLog(@"Error");
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             }
                             
                         }];
    
}

-(void)emitTheBookingACk:(NSInteger)bstatus
{
    ChatSIOClient *socket = [ChatSIOClient sharedInstance];
    NSDictionary *message=@{
                            @"bid":_dictBookingDetails[@"bid"],
                            @"bstatus":[NSNumber numberWithInteger:bstatus],
                            @"cid":_dictBookingDetails[@"cid"],
                            @"proid":[[NSUserDefaults standardUserDefaults] objectForKey:@"ProviderId"],
                            @"dt":[Helper getCurrentDateTime]
                            };
    [socket publishToChannel:@"LiveBookingAck" message:message];
}

/**
 *  get the current date
 *
 *  @return returns formatted time
 */
-(NSString *)getCurrentTimeDate
{
    
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd-HH:mm:ss"];
    NSString *dateInStringFormated = [dateFormatter stringFromDate:now];
    NSLog(@"date in string %@ ",dateInStringFormated);
    return dateInStringFormated;
    
}



- (IBAction)retake:(id)sender {
    [_signView erase];
    _isSignatureDone = NO;
    _invoiceImageURL = nil;
}

#pragma mark - TextFields

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self moveViewUp:textField andKeyboardHeight:250];
    if (textField==_miscFees) {
        _miscFees.text=@"";
        
    }else if (textField==_materialFees){
        _materialFees.text=@"";
        
    }else{
        _proDiscount.text=@"";
        
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(textField==_materialFees||textField==_miscFees||textField==_proDiscount){
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        NSString    *regex     = @"\\d{0,3}(\\.\\d{0,2})?";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
        return [predicate evaluateWithObject:newString];
    }
    return YES;
}



- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    
    if (textField == _materialFees) {
        _matFee=[textField.text integerValue];
        NSNumber *mateFee =[NSNumber numberWithInteger:[textField.text integerValue]];
        NSString *totalMat = [formatter stringFromNumber:mateFee];
        _materialFees.text=totalMat;
    }else if (textField == _miscFees) {
        _miscFee=[textField.text integerValue];
        NSNumber *miscFee =[NSNumber numberWithInteger:[textField.text integerValue]];
        NSString *totMisc = [formatter stringFromNumber:miscFee];
        _miscFees.text=totMisc;
    }else if (textField == _proDiscount) {
        _proDisc=[textField.text integerValue];
        NSNumber *proFee =[NSNumber numberWithInteger:[textField.text integerValue]];
        NSString *totPro = [formatter stringFromNumber:proFee];
        _proDiscount.text=totPro;
        
        if (_proDisc>_miscFee+_matFee) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message",@"Message") message:NSLocalizedString(@"Provider Discount shouldn't be greater than material and miscellaneous fees",@"Provider Discount shouldn't be greater than material and miscellaneous fees") delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            alert.tag=2;
            [alert show];
            
        }
    }
    
    
    NSNumber *Total =[NSNumber numberWithInteger:[flStrForObj(_invoiceDetails[@"visitAmt"])integerValue]+_miscFee+_matFee-_proDisc+[flStrForObj(_invoiceDetails[@"totalAmt"]) integerValue]-[flStrForObj(_invoiceDetails[@"discount"]) integerValue]];
    NSString *TotalAmt = [formatter stringFromNumber:Total];
    
    _totalAmt.text=TotalAmt;
    
    _mainTotalAmt=[Total integerValue];
    
    
    
    
    NSNumber *subTotAmt =[NSNumber numberWithInteger:[flStrForObj(_invoiceDetails[@"visitAmt"])integerValue]+_miscFee+_matFee-_proDisc+[flStrForObj(_invoiceDetails[@"totalAmt"]) integerValue]];
    NSString *subTot = [formatter stringFromNumber:subTotAmt];
    
    _subTotal.text=subTot;
    
    _subTotalAmt=[subTotAmt integerValue];
}


- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}
-(void) keyboardWillShow:(NSNotification *)note
{
    //    NSDictionary *userInfo = [note userInfo];
    //    CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    //
    //    NSLog(@"Keyboard Height: %f Width: %f", kbSize.height, kbSize.width);
    //
    //    // move the view up by 30 pts
    //    CGRect frame = self.view.frame;
    //    frame.origin.y = -30;
    //
    //    [UIView animateWithDuration:0.3 animations:^{
    //        self.view.frame = frame;
    //    }];
}

-(void) keyboardWillHide:(NSNotification *)note
{
    //    CGRect frame = self.view.frame;
    //    frame.origin.y = 64;
    //
    //    [UIView animateWithDuration:0.3 animations:^{
    //        self.view.frame = frame;
    //    }];
}

#pragma TextView Delegate methods
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    textView.text=@"";
    [self moveView:textView andKeyboardHeight:250];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    
    return YES;
}


-(void)textViewDidEndEditing:(UITextView *)textView
{
    _notes.text=textView.text;
    if ([_notes.text isEqual:@""]) {
        _notes.text=@"Enter Required Notes";
    }
    //   [self backToMainView];
}


- (void)moveView:(UITextView *)textview andKeyboardHeight:(float)height
{
    float viewMaxY =CGRectGetMinY([[[textview superview] superview] superview].frame);
    viewMaxY = viewMaxY + CGRectGetMinY([[textview superview] superview] .frame);
    viewMaxY = viewMaxY + CGRectGetMinY([textview superview].frame);
    viewMaxY = viewMaxY + CGRectGetMaxY(textview.frame);
    
    float textfieldMaxY = CGRectGetMinY(_contentScrollView.frame) + viewMaxY + height - _initialOffsetY;
    float reminder = textfieldMaxY - CGRectGetHeight(self.view.frame);
    
    
    NSLog(@"Reminder : %f",reminder);
    
    float offsetY;
    if (reminder > 0)
        offsetY = _initialOffsetY + reminder;
    else
        offsetY = _initialOffsetY;
    
    NSLog(@"OffsetY : %f",offsetY);
    
    [UIView animateWithDuration:0.4
                     animations:^{
                         _mainScrollView.contentOffset = CGPointMake(0, offsetY);
                     }];
}

/**
 *  Scroll up when keyboard appears
 */
- (void)moveViewUp:(UITextField *)textfield andKeyboardHeight:(float)height
{
    float viewMaxY =  CGRectGetMinY([[[textfield superview] superview] superview].frame);
    viewMaxY = viewMaxY + CGRectGetMinY([[textfield superview] superview] .frame);
    viewMaxY = viewMaxY + CGRectGetMinY([textfield superview].frame);
    viewMaxY = viewMaxY + CGRectGetMaxY(textfield.frame);
    
    float textfieldMaxY = CGRectGetMinY(_contentScrollView.frame) + viewMaxY + height - _initialOffsetY;
    float reminder = textfieldMaxY - CGRectGetHeight(self.view.frame);
    
    
    NSLog(@"Reminder : %f",reminder);
    
    float offsetY;
    if (reminder > 0)
        offsetY = _initialOffsetY + reminder;
    else
        offsetY = _initialOffsetY;
    
    NSLog(@"OffsetY : %f",offsetY);
    
    [UIView animateWithDuration:0.4
                     animations:^{
                         _mainScrollView.contentOffset = CGPointMake(0, offsetY);
                     }];
}
/**
 *  Scroll up when keyboard hides
 */

- (void)moveViewDown
{
    
    [UIView animateWithDuration:0.4
                     animations:^{
                         CGPoint offset;
                         offset.x = 0;
                         offset.y = 0;
                         _mainScrollView.contentOffset = offset;
                     }];
    
}

@end
