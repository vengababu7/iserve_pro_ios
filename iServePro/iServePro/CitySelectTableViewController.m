//
//  CitySelectTableViewController.m
//  iServePro
//
//  Created by Rahul Sharma on 25/04/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "CitySelectTableViewController.h"
#import "LocationTracker.h"
#import "CitySelectTableViewCell.h"

@interface CitySelectTableViewController ()<CLLocationManagerDelegate>

@property (nonatomic, strong) NSMutableArray *arrayProvide;
@property (nonatomic, strong) NSMutableArray *arrayOfSearch;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *cancel;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *Add;
@property(nonatomic,assign)float currentLatitude;
@property(nonatomic,assign)float currentLongitude;
@property (strong, nonatomic) IBOutlet UITableView *citySelectTableView;

@end

@implementation CitySelectTableViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    CLLocationManager *lm = [[CLLocationManager alloc] init];
    lm.delegate = self;
    lm.desiredAccuracy = kCLLocationAccuracyBest;
    lm.distanceFilter = kCLDistanceFilterNone;
    [lm startUpdatingLocation];
    
    
    _currentLatitude=13.3445;
    _currentLongitude=77.3243;
    [self.cancel setTitle:@"Cancel"];
    [self.Add setTitle:@" "];
    [self.Add setTintColor:UIColorFromRGB(0x000000)];
    [self serviceForTypes];
    // [self UpdateAddButtonTitle ];
    self.tableView.allowsMultipleSelectionDuringEditing = YES;
    self.navigationItem.leftBarButtonItem =self.cancel;
    self.navigationItem.rightBarButtonItem = self.Add;
    [self createNavLeftButton];
    
    
}
/*-----------------------------------*/
#pragma mark - Navigation Bar methods
/*-----------------------------------*/
- (void)createNavLeftButton
{
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton setImage:[UIImage imageNamed:@"bnt_bck_normal"]
                     forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:@"bnt_bck_pressed"]
                     forState:UIControlStateSelected];
    
    [navCancelButton addTarget:self action:@selector(backToController) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(10.0f,0.0f,40,40)];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -14;// it was -6 in iOS 6
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void)backToController
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)serviceForTypes
{
    NSDictionary *queryParams = @{
                                  
                                  @"ent_date_time":[Helper getCurrentDateTime]
                                  
                                  };
    TELogInfo(@"param%@",queryParams);
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator showPIOnView:self.view withMessage:@"Getting Cities.."];
    
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    
    [handler composeRequestWithMethod:@"getCityList"
                              paramas:queryParams
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             [[ProgressIndicator sharedInstance]hideProgressIndicator];
                             
                             if (!response) {
                                 return;
                             }
                             if (succeeded) {
                                 [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                 _arrayProvide = response[@"city"];
                                 [self reloadTableInAnimation];
                                 // [_citySelectTableView reloadData];
                                 
                                 [self updateButtonsToMatchTableState];
                             }
                             else{
                                 
                                 [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                 
                             }
                         }];
    
}


#pragma mark - UITableViewDelegate


- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //  [self UpdateAddButtonTitle];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_arrayOfSearch.count==0) {
        return self.arrayProvide.count;
    }else{
        return self.arrayOfSearch.count;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self addActiom:nil];
    //  [self UpdateAddButtonTitle];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *kCellID = @"cityCell";
    CitySelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    if (_arrayOfSearch.count==0) {
        
        NSString *myString=[self.arrayProvide objectAtIndex:indexPath.row][@"City_Name"];
        NSString *lower = [myString uppercaseString];
        
        cell.selectedCity.text =lower;
        cell.selectedCityID = [NSString stringWithFormat:@"%@",[self.arrayProvide objectAtIndex:indexPath.row][@"city_ID"]];
    }else{
        NSString *myString=[self.arrayOfSearch objectAtIndex:indexPath.row][@"City_Name"];
        NSString *lower = [myString uppercaseString];
        
        cell.selectedCity.text = lower;
        cell.selectedCityID = [NSString stringWithFormat:@"%@",[self.arrayOfSearch objectAtIndex:indexPath.row][@"city_ID"]];
    }
    
    return cell;
}

#pragma mark - SearchBar Delegates


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [self filterContent:searchBar.text];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    //[self cancelSearch];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self filterContent:searchBar.text];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [self.view endEditing:YES];
}
/*  Filter the search content
 *
 *  @param searchText search text
 */
- (void)filterContent:(NSString*)searchText
{
    _arrayOfSearch = [[NSMutableArray alloc] init];
    // Populate the results
    for (NSDictionary *tempDict in _arrayProvide ) {
        
        if ([tempDict[@"City_Name"] rangeOfString:searchText
                                          options:(NSCaseInsensitiveSearch)].location == NSNotFound) {
            // Substring Not Found
        }
        else {
            // Substring Found Successfully
            [_arrayOfSearch addObject:tempDict];
        }
    }
    NSLog(@"Search result array : %@",_arrayOfSearch);
    [self reloadTableInAnimation];
}
- (void)reloadTableInAnimation {
    
    NSRange range = NSMakeRange(0, [self numberOfSectionsInTableView:self.citySelectTableView]);
    NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:range];
    [self.citySelectTableView reloadSections:sections withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Updating button state

- (void)updateButtonsToMatchTableState
{
    
    self.navigationItem.rightBarButtonItem = self.Add;
    
}

- (void)UpdateAddButtonTitle
{
    
    NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
    
    if (selectedRows.count == 1) {
        self.Add.title = NSLocalizedString(@"Add", @"");
    }
    else
    {
        self.Add.title = NSLocalizedString(@"Select", @"");
    }
}


- (IBAction)CancelAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)addActiom:(id)sender {
    if ([[self.tableView indexPathsForSelectedRows] count]) {
        NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
        NSString *city;
        NSString *city_id;
        NSUserDefaults *userDefaults =[NSUserDefaults standardUserDefaults];
        for (NSIndexPath *selectionIndex in selectedRows)
        {
            if (_arrayOfSearch.count==0) {
                city_id =[self.arrayProvide objectAtIndex:selectionIndex.row][@"City_Id"];
                city =[self.arrayProvide objectAtIndex:selectionIndex.row][@"City_Name"];
                [userDefaults setObject:city_id forKey:@"city_ID"];
                [userDefaults synchronize];
            }else{
                city_id =[self.arrayOfSearch objectAtIndex:selectionIndex.row][@"City_Id"];
                city =[self.arrayOfSearch objectAtIndex:selectionIndex.row][@"City_Name"];
                [userDefaults setObject:city_id forKey:@"city_ID"];
                [userDefaults synchronize];
            }
            
        }
        if (self.delegate && [self.delegate respondsToSelector:@selector(SelectedCity:)]) {
            [self.delegate SelectedCity:city];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [Helper showAlertWithTitle:@"Message" Message:@"You Haven't Selected Any City"];
    }
    
}



@end
