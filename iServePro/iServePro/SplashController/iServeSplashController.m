

#import "iServeSplashController.h"
#import "iServeHelpController.h"
#import "HomeTabBarController.h"


@interface iServeSplashController ()

@end

@implementation iServeSplashController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    self.navigationItem.hidesBackButton = YES;
    self.view.alpha=1.0f;

    if ([UIScreen mainScreen].bounds.size.height == 568) {
        
        splashImage.image = [UIImage imageNamed:@"default_568h"];
    }
    else if ([UIScreen mainScreen].bounds.size.height == 480)
    {
        splashImage.image = [UIImage imageNamed:@"default"];
    }
    else if ([UIScreen mainScreen].bounds.size.height == 667)
    {
        splashImage.image = [UIImage imageNamed:@"Default.png"];
        
    }else if ([UIScreen mainScreen].bounds.size.height == 736)
    {
        splashImage.image = [UIImage imageNamed:@"default_768h.png"];
    }
    else {
        splashImage.image = [UIImage imageNamed:@"default_768h.png"];
    }
}


-(void)viewDidAppear:(BOOL)animated
{
    
    NSString *deviceId;
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        deviceId = [oNSUUID UUIDString];
    } else
    {
        deviceId = [oNSUUID UUIDString];
        
    }
    [[NSUserDefaults standardUserDefaults] setObject:deviceId forKey:kPMDDeviceIdKey];
    
    
    [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(removeSplash) userInfo:Nil repeats:NO];
    
}


-(void)removeSplash
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken]&& [[[NSUserDefaults standardUserDefaults] objectForKey:@"signing"] integerValue] == 0)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        //[(NSMutableArray*)self.navigationController.viewControllers removeAllObjects];
       HomeTabBarController  *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    }
    else
    {
        // [self performSegueWithIdentifier:@"Help" sender:self];
        iServeHelpController *help = [self.storyboard instantiateViewControllerWithIdentifier:@"helpVC"];
        [[self navigationController ] pushViewController:help animated:NO];
        
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"menuSegue"])
    {
        
        HomeTabBarController *MPC = [[HomeTabBarController alloc]init];
        
        MPC =[segue destinationViewController];
        
        
    }
    else
    {
        iServeHelpController *HVC = [[iServeHelpController alloc]init];
        
        HVC =[segue destinationViewController];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) getDirection
{
    
}

@end
