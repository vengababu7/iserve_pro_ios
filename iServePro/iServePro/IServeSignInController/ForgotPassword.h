//
//  ForgotPassword.h
//  iServePro
//
//  Created by Rahul Sharma on 01/10/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPassword : UIViewController
- (IBAction)submitPassword:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *forgotPassword;
@end
