//
//  ForgotPassword.m
//  iServePro
//
//  Created by Rahul Sharma on 01/10/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "ForgotPassword.h"

@interface ForgotPassword ()<UIGestureRecognizerDelegate>

@end

@implementation ForgotPassword

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createNavLeftButton];
    self.title=@"Retrive Password";
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissView)];
    tapGesture.delegate = self;
    [self.view addGestureRecognizer:tapGesture];
    [_forgotPassword becomeFirstResponder];
}
-(void)dismissView{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*-----------------------------------*/
#pragma mark - Navigation Bar methods
/*-----------------------------------*/
- (void)createNavLeftButton
{
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton setImage:[UIImage imageNamed:@"current_booking_cross_icon_off"]
                     forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:@"current_booking_cross_icon_on"]
                     forState:UIControlStateSelected];
    
    [navCancelButton addTarget:self action:@selector(backToController) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(10.0f,0.0f,40,40)];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}
-(void)backToController
{
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)retrievePassword:(NSString *)text
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    NSDictionary *dict =@{
                          @"ent_email":_forgotPassword.text,
                          @"ent_user_type":@"1"
                          };
    NetworkHandler *handler =[NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:@"forgotPassword"
                              paramas:dict
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             if (succeeded) {
                                 [self retrievePasswordResponse:response];
                             }
                         }];
}

- (IBAction)submitPassword:(id)sender {
    [self.view endEditing:YES];
    NSLog(@"Email Name: %@", _forgotPassword.text);
    if (_forgotPassword.text.length) {
        
        if (((unsigned long)_forgotPassword.text.length ==0) || [Helper emailValidationCheck:_forgotPassword.text] == 0)
        {
            [Helper showAlertWithTitle:@"Invalid Email ID" Message:@"Re enter your email ID"];
        }
        else
        {
            [self retrievePassword:_forgotPassword.text];
        }
    }else{
        [Helper showAlertWithTitle:@"Message" Message:@"Enter Email ID"];
    }
    
}

-(void)retrievePasswordResponse :(NSDictionary *)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    NSLog(@"response:%@",response);
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        _forgotPassword.text =@"";
        
    }
    else
    {
        if ([[response objectForKey:@"errFlag"] intValue] == 0)
        {
            [Helper showAlertWithTitle:@"Message" Message:[response objectForKey:@"errMsg"]];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else
        {
            [Helper showAlertWithTitle:@"Message" Message:[response objectForKey:@"errMsg"]];
        }
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self submitPassword:nil];
    return YES;
}


@end
