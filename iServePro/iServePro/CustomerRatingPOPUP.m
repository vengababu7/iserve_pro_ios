//
//  CustomerRatingPOPUP.m
//  iServePro
//
//  Created by Rahul Sharma on 03/05/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "CustomerRatingPOPUP.h"

@implementation CustomerRatingPOPUP


-(id)init{
    self = [[[NSBundle mainBundle] loadNibNamed:@"CustomerRating"
                                          owner:self
                                        options:nil] firstObject];
    return self;
}
-(void) onWindow:(UIWindow *)onWindow
{
      _isSelected=NO;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dimissView)];
    tapGesture.delegate = self;
    [self addGestureRecognizer:tapGesture];
    self.frame = onWindow.frame;
    [onWindow addSubview:self];
    
    
    [self layoutIfNeeded];
    self.contentView.alpha = 0.3;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.contentView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                     }];
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:_contentView]) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }
    
    return YES;
}

-(void)ReasonTags:(NSInteger)tag
{
    _isSelected=YES;
    switch (tag) {
        case 1:
            reason=1;
            break;
        case 2:
            reason=2;
            break;
        case 3:
            reason=3;
            break;
        case 4:
            reason=4;
            break;
        default:
            break;
    }
}
-(void)dimissView{
    self.contentView.alpha = 1;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.contentView.alpha = 0.3;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
    
}
- (IBAction)submitTheRating:(id)sender{
    if (reason ==0) {
        [Helper showAlertWithTitle:@"Message" Message:@"Please Select Any Reason"];
    }
    else{
    self.contentView.alpha = 1;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.contentView.alpha = 0.3;
                     }
                     completion:^(BOOL finished) {
                         if (_isSelected) {
                               [_delegate popUpRatingOFfDismiss:reason];
                         }
                         [self removeFromSuperview];
                     }];
    }
    
}


- (IBAction)reason1:(id)sender {
    _cancelReason1.selected=YES;
    _cancelReason2.selected=NO;
    _cancelReason3.selected=NO;
    _cancelReason4.selected=NO;
    [self ReasonTags:_cancelReason1.tag];
}

- (IBAction)reason2:(id)sender {
    _cancelReason1.selected=NO;
    _cancelReason2.selected=YES;
    _cancelReason3.selected=NO;
    _cancelReason4.selected=NO;
    [self ReasonTags:_cancelReason2.tag];
    
}

- (IBAction)reason3:(id)sender {
    _cancelReason1.selected=NO;
    _cancelReason2.selected=NO;
    _cancelReason3.selected=YES;
    _cancelReason4.selected=NO;
    [self ReasonTags:_cancelReason3.tag];
    
}
- (IBAction)reason4:(id)sender {
    _cancelReason1.selected=NO;
    _cancelReason2.selected=NO;
    _cancelReason3.selected=NO;
    _cancelReason4.selected=YES;
    [self ReasonTags:_cancelReason4.tag];
    
}
@end
