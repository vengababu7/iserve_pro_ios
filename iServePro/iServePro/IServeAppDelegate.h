//
//  AppDelegate.h
//  iServePro
//
//  Created by Rahul Sharma on 24/02/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"Reachability.h"
#import<CoreData/CoreData.h>

@interface IServeAppDelegate : UIResponder <UIApplicationDelegate>
{
@private
    NSManagedObjectContext *managedObjectContext_;
    NSManagedObjectModel *managedObjectModel_;
    NSPersistentStoreCoordinator *persistentStoreCoordinator_;

}

@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, readonly) int networkStatus;

- (NSURL *)applicationDocumentsDirectory;

/**
 *  Check for network Connection
 *
 *  @return Yes if network is available
 */
- (BOOL)isNetworkAvailable;
-(void)checkDriverStatus;
-(void)openNewBookingVCWithInfo:(NSDictionary *)info;
//-(void)subscribeToPubnubChannel;



@end

